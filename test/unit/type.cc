// Copyright (c) 2018-2019, authors of Sekreta. See AUTHORS.md
//       or use the `git log` command on this file.
//
//    Licensed under the Sekreta License, Version 1.0,
//  which inherits from the Apache License, Version 2.0.
//   Combined, they are described as a single "License".
//
//         See the LICENSE.md file for details.

#include "type.hh"

#include <mutex>
#include <string>
#include <thread>
#include <variant>
#include <vector>

#include "gtest/gtest.h"

namespace type = sekreta::type;
namespace crypto = type::crypto;

// UID

struct UIDFixture : ::testing::Test
{
 protected:
  std::string name = "webserver";
  uint32_t num = 987654321;
  using t_uid = type::UID<std::string, uint32_t>;
};

TEST_F(UIDFixture, NameCtor)
{
  t_uid one{name};
  EXPECT_EQ(one.name(), name);

  t_uid two(name, num);
  EXPECT_EQ(two.name(), name);
  EXPECT_EQ(two.num(), num);
}

TEST_F(UIDFixture, PairCtor)
{
  auto pair = std::make_pair(name, num);
  t_uid uid{pair};

  EXPECT_EQ(uid.name(), pair.first);
  EXPECT_EQ(uid.num(), pair.second);
}

TEST_F(UIDFixture, Functor)
{
  auto pair = std::make_pair(name, num);
  t_uid uid{pair};

  EXPECT_EQ(uid(), pair);
}

TEST_F(UIDFixture, CopyAssignment)
{
  t_uid one{std::make_pair(name, num)};
  t_uid two{"name", 123};

  two = one;

  EXPECT_EQ(two(), one());
  EXPECT_EQ(two.name(), one.name());
  EXPECT_EQ(two.num(), one.num());
}

TEST_F(UIDFixture, CopyCtor)
{
  t_uid one{std::make_pair(name, num)};
  t_uid two{one};

  EXPECT_EQ(two(), one());
  EXPECT_EQ(two.name(), one.name());
  EXPECT_EQ(two.num(), one.num());
}

TEST_F(UIDFixture, NPI)
{
  t_uid uid;
  uid.name(name).num(num);

  EXPECT_EQ(uid.name(), name);
  EXPECT_EQ(uid.num(), num);
}

TEST_F(UIDFixture, Operator_SingleName)
{
  type::UID<> one{"uid", 1};
  type::UID<> two{"uid", 2};

  EXPECT_EQ(true, one == one);
  EXPECT_EQ(true, two == two);
  EXPECT_EQ(true, one < two);
  EXPECT_EQ(true, two > one);
  EXPECT_EQ(true, one != two);
  EXPECT_EQ(true, two != one);
  EXPECT_EQ(true, one <= two);
  EXPECT_EQ(true, two >= one);
}

TEST_F(UIDFixture, Operator_MultipleName)
{
  t_uid one{"one", 1};
  t_uid two{"two", 2};

  EXPECT_EQ(true, one == one);
  EXPECT_EQ(true, two == two);
  EXPECT_EQ(true, one < two);
  EXPECT_EQ(true, two > one);
  EXPECT_EQ(true, one != two);
  EXPECT_EQ(true, two != one);
  EXPECT_EQ(true, one <= two);
  EXPECT_EQ(true, two >= one);
}

TEST_F(UIDFixture, Operator_NumOnly)
{
  t_uid one{"", 1};
  t_uid two{"", 2};

  EXPECT_EQ(true, one == one);
  EXPECT_EQ(true, two == two);
  EXPECT_EQ(true, one < two);
  EXPECT_EQ(true, two > one);
  EXPECT_EQ(true, one != two);
  EXPECT_EQ(true, two != one);
  EXPECT_EQ(true, one <= two);
  EXPECT_EQ(true, two >= one);
}

TEST_F(UIDFixture, Operator_Sort)
{
  std::vector<t_uid> vec;

  vec.push_back({"uid", 1});
  vec.push_back({"uid", 0});
  vec.push_back({"uid", 3});
  vec.push_back({"uid", 2});

  std::sort(vec.begin(), vec.end());

  for (size_t i = 0; i < 4; i++)
    EXPECT_EQ(i, vec.at(i).num());
}

TEST_F(UIDFixture, Thread)
{
  t_uid uid{"uid"};
  std::vector<t_uid> uids;

  std::mutex mutex;
  std::vector<std::thread> threads;

  auto mutate = [&uids, &uid, &mutex](const size_t num) {
    std::scoped_lock lock(mutex);
    uid.num(num);
    uids.push_back(uid);
  };

  for (size_t i = 0; i < 10; i++)
    threads.push_back(std::thread([&mutate, i]() { mutate(i); }));

  // And also run in main thread
  for (size_t i = 10; i < 20; i++)
    mutate(i);

  for (auto& thread : threads)
    thread.join();

  std::sort(uids.begin(), uids.end());
  for (size_t i = 0; i < uids.size(); i++)
    ASSERT_EQ(i, uids.at(i).num());
}

// TODO(unassigned): more + better concurrency tests

// Port

struct PortFixture : ::testing::Test
{
 protected:
  uint16_t bound = 8080, unbound = 80;
  using t_port = type::Port<uint16_t, uint16_t>;
};

TEST_F(PortFixture, SimpleCtor)
{
  t_port port{bound, unbound};
  EXPECT_EQ(bound, port.bound());
  EXPECT_EQ(unbound, port.unbound());
}

TEST_F(PortFixture, PairCtor)
{
  t_port port{std::pair(bound, unbound)};
  EXPECT_EQ(bound, port.bound());
  EXPECT_EQ(unbound, port.unbound());
}

TEST_F(PortFixture, CopyAssignment)
{
  t_port one{std::make_pair(bound, unbound)};
  t_port two{bound, unbound};

  two = one;

  EXPECT_EQ(two(), one());
  EXPECT_EQ(two.bound(), one.bound());
  EXPECT_EQ(two.unbound(), one.unbound());
}

TEST_F(PortFixture, CopyCtor)
{
  t_port one{std::make_pair(bound, unbound)};
  t_port two{one};

  EXPECT_EQ(two(), one());
  EXPECT_EQ(two.bound(), one.bound());
  EXPECT_EQ(two.unbound(), one.unbound());
}

TEST_F(PortFixture, NPI)
{
  t_port port;
  port.bound(bound).unbound(unbound);

  EXPECT_EQ(bound, port.bound());
  EXPECT_EQ(unbound, port.unbound());
}

TEST_F(PortFixture, CustomTypeWithFunctor)
{
  struct Port
  {
    Port() = default;
    Port(const int port) : port(port) {}
    int operator()() const { return port; }
    int port;
  };

  type::Port<Port, Port> port{Port{bound}, Port{unbound}};
  EXPECT_EQ(port.bound()(), bound);
  EXPECT_EQ(port.unbound()(), unbound);
}

// Point

struct PointFixture : ::testing::Test
{
 protected:
  std::string_view dest = "127.0.0.1";
  uint16_t port = 12345;
  using t_point = type::Point<std::string_view, uint16_t>;
};

TEST_F(PointFixture, PointCtorOnly)
{
  t_point point{dest};
  EXPECT_EQ(point.dest(), dest);
}

TEST_F(PointFixture, PortCtor)
{
  using t_port = type::Port<uint16_t, uint16_t>;
  t_port port{80, 8080};
  type::Point<std::string_view, t_port> point(dest, port);

  EXPECT_EQ(point.dest(), dest);
  EXPECT_EQ(point.port().bound(), 80);
  EXPECT_EQ(point.port().unbound(), 8080);

  dest = "10.0.0.1";
  port.bound(31337);

  point.dest(dest).port(port);
  EXPECT_EQ(point.dest(), dest);
  EXPECT_EQ(point.port().bound(), 31337);
  EXPECT_EQ(point.port().unbound(), 8080);
}

TEST_F(PointFixture, PointAndCustomPortCtor)
{
  t_point point{dest, port};
  EXPECT_EQ(point.dest(), dest);
  EXPECT_EQ(point.port(), port);

  dest = "10.0.0.1";
  port = 31337;
  point.dest(dest).port(port);

  EXPECT_EQ(point.dest(), dest);
  EXPECT_EQ(point.port(), port);
}

TEST_F(PointFixture, CustomPortFunctor)
{
  // Note: the signed type used should not trigger a static assertion
  class Port
  {
    int port{};

   public:
    Port() = default;
    explicit Port(int port) : port(port) {}
    int operator()() const noexcept { return port; }
  };

  Port port{31337};
  type::Point<std::string_view, Port> point{dest};
  point.port(port);
  EXPECT_EQ(point.port()(), port());
}

TEST_F(PointFixture, CopyAssignment)
{
  t_point one{{dest, port}};
  t_point two{"longhash", 56789};

  two = one;

  EXPECT_EQ(two(), one());
  EXPECT_EQ(two.dest(), one.dest());
  EXPECT_EQ(two.port(), one.port());
}

TEST_F(PointFixture, CopyCtor)
{
  t_point one{{dest, port}};
  t_point two{one};

  EXPECT_EQ(two(), one());
  EXPECT_EQ(two.dest(), one.dest());
  EXPECT_EQ(two.port(), one.port());
}

TEST_F(PointFixture, NPI)
{
  type::Point<std::string_view, type::Port<uint16_t, uint16_t>> point;

  point.dest("127.0.0.1");
  point.port({12345, 54321});

  EXPECT_EQ(point.dest(), dest);
  EXPECT_EQ(point.port().bound(), 12345);
  EXPECT_EQ(point.port().unbound(), 54321);
}

TEST(Point, Complex)
{
  using t_hash = std::vector<std::byte>;
  using t_dest = std::variant<std::string, t_hash>;
  using t_port = std::variant<std::string, uint16_t>;

  t_hash dest{std::byte{0x41}, std::byte{0x42}};
  std::string port("31337");

  type::Point<t_dest, t_port> point(dest, port);

  EXPECT_EQ(std::get<t_hash>(point.dest()), dest);
  EXPECT_EQ(std::get<std::string>(point.port()), port);

  auto hash = std::get_if<std::string>(&point.dest());
  EXPECT_EQ(hash, nullptr);

  auto num = std::get_if<uint16_t>(&point.port());
  EXPECT_EQ(num, nullptr);
}

TEST(Point, ComplexAssignment)
{
  using t_hash = std::vector<std::byte>;

  using t_dest = std::variant<std::string, t_hash>;
  using t_port = std::variant<std::string, uint16_t>;
  using t_point = type::Point<t_dest, t_port>;

  t_hash dest{std::byte{0x41}, std::byte{0x42}};
  std::string port("56789");

  t_point point(dest, port);
  t_point other(t_hash{std::byte{0x43}, std::byte{0x44}}, "12345");

  other = point;

  EXPECT_EQ(std::get<t_hash>(other.dest()), dest);
  EXPECT_EQ(std::get<std::string>(other.port()), port);
}

TEST_F(PointFixture, PortClass)
{
  type::Point<std::string_view, type::Port<uint16_t, uint16_t>> point;
  point.dest(dest).port({80, 8080});

  EXPECT_EQ(dest, point.dest());
  EXPECT_EQ(80, point.port().bound());
  EXPECT_EQ(8080, point.port().unbound());
}

// ACL

struct ACLFixture : ::testing::Test
{
 protected:
  std::string_view white = "white", black = "black";
  using t_acl = type::ACL<std::string_view>;
};

TEST_F(ACLFixture, CtorOnly)
{
  t_acl acl{white, black};
  EXPECT_EQ(acl.white_list(), white);
  EXPECT_EQ(acl.black_list(), black);
}

TEST_F(ACLFixture, CtorOnlyContainer)
{
  t_acl acl{std::make_pair(white, black)};
  EXPECT_EQ(acl.white_list(), white);
  EXPECT_EQ(acl.black_list(), black);
}

TEST_F(ACLFixture, NPI)
{
  t_acl acl;
  acl.white_list(white).black_list(black);

  EXPECT_EQ(acl.white_list(), white);
  EXPECT_EQ(acl.black_list(), black);
}

TEST_F(ACLFixture, CustomCtor)
{
  using t_list = std::vector<uint8_t>;
  type::ACL<t_list, t_list> acl{{0x00, 0x01}, {0x02, 0x03}};

  EXPECT_EQ(acl.white_list(), (t_list{0x00, 0x01}));
  EXPECT_EQ(acl.black_list(), (t_list{0x02, 0x03}));

  acl.white_list({0x04, 0x05}).black_list({0x06, 0x07});
  EXPECT_EQ(acl.white_list(), (t_list{0x04, 0x05}));
  EXPECT_EQ(acl.black_list(), (t_list{0x06, 0x07}));
}

TEST_F(ACLFixture, CopyAssignment)
{
  t_acl one{white, black};
  t_acl two{"white", "black"};

  two = one;

  EXPECT_EQ(two(), one());
  EXPECT_EQ(two.white_list(), one.white_list());
  EXPECT_EQ(two.black_list(), one.black_list());
}

TEST_F(ACLFixture, CopyCtor)
{
  t_acl one{white, black};
  t_acl two{one};

  EXPECT_EQ(two(), one());
  EXPECT_EQ(two.white_list(), one.white_list());
  EXPECT_EQ(two.black_list(), one.black_list());
}

// PathType

struct PathTypeFixture : ::testing::Test
{
 protected:
  using t_direction = type::kPathDirection;
  using t_affect = type::kPathAffect;
  using t_path = type::PathType<t_direction, t_affect>;

  t_direction direction = t_direction::Client;
  t_affect affect = t_affect::Default;
};

TEST_F(PathTypeFixture, CtorOnly)
{
  t_path path{direction, affect};
  EXPECT_EQ(path.direction(), direction);
  EXPECT_EQ(path.affect(), affect);
}

TEST_F(PathTypeFixture, CtorOnlyContainer)
{
  t_path path{{direction, affect}};
  EXPECT_EQ(path.direction(), direction);
  EXPECT_EQ(path.affect(), affect);
}

TEST_F(PathTypeFixture, NPI)
{
  t_path path;
  path.direction(direction).affect(affect);
  EXPECT_EQ(path.direction(), direction);
  EXPECT_EQ(path.affect(), affect);
}

TEST_F(PathTypeFixture, CustomCtor)
{
  using t_param = std::string_view;
  type::PathType<t_param, t_param> path{"client", "default"};

  EXPECT_EQ(path.direction(), "client");
  EXPECT_EQ(path.affect(), "default");

  path.direction("server").affect("HTTP");
  EXPECT_EQ(path.direction(), "server");
  EXPECT_EQ(path.affect(), "HTTP");
}

TEST_F(PathTypeFixture, CopyAssignment)
{
  t_path one{direction, affect};
  t_path two{t_direction::Server, t_affect::IRC};

  two = one;

  EXPECT_EQ(two(), one());
  EXPECT_EQ(two.direction(), one.direction());
  EXPECT_EQ(two.affect(), one.affect());
}

TEST_F(PathTypeFixture, CopyCtor)
{
  t_path one{direction, affect};
  t_path two{one};

  EXPECT_EQ(two(), one());
  EXPECT_EQ(two.direction(), one.direction());
  EXPECT_EQ(two.affect(), one.affect());
}

// Key

struct KeyFixture : ::testing::Test
{
 protected:
  using t_data = std::vector<uint8_t>;
};

TEST_F(KeyFixture, CryptoSimple) {}

TEST_F(KeyFixture, CryptoComplex)
{
  class FileSystem final
  {
    std::string m_file;

   public:
    explicit FileSystem(const std::string& file) : m_file(file) {}

   public:
    bool operator==(const FileSystem& rhs) const
    {
      return this->m_file == rhs.m_file;
    }

   public:
    void operator()(const std::string& file) { m_file = file; }
    const std::string& operator()() const noexcept { return m_file; }
  };

  using t_cipher = crypto::kScheme;
  using t_type = crypto::kType;
  using t_key =
      crypto::Key<std::tuple<t_data, std::pair<t_cipher, t_type>, FileSystem>>;
  using t_map = std::multimap<t_cipher, t_key>;

  // Keypairs will almost certainly never be separated, but this is a "complex"
  //   test unit :)
  t_map map;

  map.insert({t_cipher::ElGamal,
              {{0x00, 0x01, 0x02},
               {t_cipher::ElGamal, t_type::Public},
               FileSystem{"/tmp/pub.data"}}});

  map.insert({t_cipher::ElGamal,
              {{0x03, 0x04, 0x05},
               {t_cipher::ElGamal, t_type::Secret},
               FileSystem{"/tmp/priv.data"}}});

  auto range = map.equal_range(t_cipher::ElGamal);
  for (auto i = range.first; i != range.second; i++)
    {
      if (i->second.file() == FileSystem{"/tmp/priv.data"})
        {
          EXPECT_EQ(
              i->second.type(),
              (std::make_pair(t_cipher::ElGamal, t_type::Secret)));
          EXPECT_EQ(i->second.data(), (t_data{0x03, 0x04, 0x05}));
        }
    }
}

TEST_F(KeyFixture, KeyPairAndSingleKey)
{
  crypto::Key<crypto::KeyPair<t_data>> pair;
  pair().pk({0x01, 0x02, 0x03}).sk({0x04, 0x05, 0x06});

  crypto::Key<t_data> key{{0x07, 0x08, 0x09}};
  key().push_back(0x10);
  EXPECT_EQ(key(), (t_data{0x07, 0x08, 0x09, 0x10}));

  pair().pk(key());
  EXPECT_EQ(pair().pk(), key());

  key().pop_back();
  EXPECT_EQ(key(), (t_data{0x07, 0x08, 0x09}));
}

TEST_F(KeyFixture, KeyVariant)
{
  crypto::Key<std::variant<std::string, t_data>> key;
}

TEST_F(KeyFixture, SuperKey) {}

// HopsType

struct HopsTypeFixture : ::testing::Test
{
 protected:
  uint8_t count = 3;
  std::string_view tag{"tag"};
  using t_hops = type::HopsType<uint8_t, std::string_view>;
};

TEST_F(HopsTypeFixture, SimpleCtor)
{
  t_hops hops(count, tag);
  EXPECT_EQ(count, hops.count());
  EXPECT_EQ(tag, hops.tag());
}

TEST_F(HopsTypeFixture, CtorSeparate)
{
  t_hops hops(count, tag);
  EXPECT_EQ(count, hops.count());
  EXPECT_EQ(tag, hops.tag());
}

TEST_F(HopsTypeFixture, CtorContained)
{
  t_hops hops(t_hops{count, tag});
  EXPECT_EQ(count, hops.count());
  EXPECT_EQ(tag, hops.tag());
}

TEST_F(HopsTypeFixture, CopyAssignment)
{
  t_hops one{count, tag};
  t_hops two;

  two = one;

  EXPECT_EQ(two(), one());
  EXPECT_EQ(two.count(), one.count());
  EXPECT_EQ(two.tag(), one.tag());
}

TEST_F(HopsTypeFixture, CopyCtor)
{
  t_hops one{count, tag};
  t_hops two{one};

  EXPECT_EQ(two(), one());
  EXPECT_EQ(two.count(), one.count());
  EXPECT_EQ(two.tag(), one.tag());
}

TEST_F(HopsTypeFixture, NPI)
{
  t_hops hops;
  hops.count(count).tag(tag);

  EXPECT_EQ(count, hops.count());
  EXPECT_EQ(tag, hops.tag());
}

// Hops

struct HopsFixture : ::testing::Test
{
 protected:
  uint8_t count = 3;
  std::string_view itag{"inbound tag"}, otag{"outbound tag"};

  using t_hop = type::HopsType<uint8_t, std::string_view>;
  t_hop inbound{count, itag};
  t_hop outbound{count, otag};

  using t_hops = type::Hops<t_hop>;
};

TEST_F(HopsFixture, HopsInboundOnly)
{
  t_hops hops{{count, itag}};
  ASSERT_EQ(hops.inbound(), inbound);
}

TEST_F(HopsFixture, HopsInboundOutboundSeparate)
{
  t_hops hops{{count, itag}, {count, otag}};
  ASSERT_EQ(hops.inbound(), inbound);
  ASSERT_EQ(hops.outbound(), outbound);
}

TEST_F(HopsFixture, HopsInboundOutboundContained)
{
  t_hops hops{t_hops{{count, itag}, {count, otag}}};
  ASSERT_EQ(hops.inbound(), inbound);
  ASSERT_EQ(hops.outbound(), outbound);
}

TEST_F(HopsFixture, CopyAssignment)
{
  t_hops one{t_hops{{count, itag}}};
  t_hops two{t_hops{{2, "second"}}};

  two = one;

  ASSERT_EQ(two(), one());
  ASSERT_EQ(two.inbound(), one.inbound());

  ASSERT_EQ(two.inbound().count(), count);
  ASSERT_EQ(two.inbound().tag(), itag);
}

TEST_F(HopsFixture, CopyCtor)
{
  t_hops one{t_hops{{count, itag}}};
  t_hops two{one};

  ASSERT_EQ(two(), one());
  ASSERT_EQ(two.inbound(), one.inbound());

  ASSERT_EQ(two.inbound().count(), count);
  ASSERT_EQ(two.inbound().tag(), itag);
}

TEST_F(HopsFixture, NPI)
{
  t_hops hops;

  hops.inbound({count, itag}).outbound({count, otag});

  ASSERT_EQ(hops.inbound(), inbound);
  ASSERT_EQ(hops.outbound(), outbound);

  ASSERT_EQ(hops.inbound().count(), count);
  ASSERT_EQ(hops.inbound().tag(), itag);

  ASSERT_EQ(hops.outbound().count(), count);
  ASSERT_EQ(hops.outbound().tag(), otag);
}

// Path

struct PathFixture : ::testing::Test
{
 protected:
  using t_path = type::Path<>;
};

TEST_F(PathFixture, TunnelCopyAssignment)
{
  t_path path;
  t_path other = path.uid({"uid"});

  auto one = other.build();
  EXPECT_EQ(one->uid().name(), "uid");

  one->uid({"other"});
  EXPECT_EQ(one->uid().name(), "other");

  auto two = path.build();
  EXPECT_EQ(two->uid().name(), "uid");

  auto three = other.build();  // already been built, not been set again
  EXPECT_EQ(three->uid().name(), "");
}

TEST_F(PathFixture, TunnelCopyCtor)
{
  t_path path;
  path.uid({"uid"});

  t_path other(path);

  auto one = other.build();
  EXPECT_EQ(one->uid().name(), "uid");

  one->uid({"other"});
  EXPECT_EQ(one->uid().name(), "other");

  auto two = path.build();
  EXPECT_EQ(two->uid().name(), "uid");

  auto three = other.build();  // already been built, not been set again
  EXPECT_EQ(three->uid().name(), "");
}

TEST_F(PathFixture, TunnelBuilder)
{
  auto path = std::make_shared<t_path>();

  auto tunnel =
      path->uid({"my path tunnel"})
          .type({type::kPathDirection::Client, type::kPathAffect::Default})
          .local_point({"hostname", {12345, {}}})
          .remote_point({"desthash", 31337})
          .hops({{3, "inbound options"}, {3, "outbound options"}})
          .crypto(
              {{std::byte{0x00}}, crypto::kScheme::Ed25519, "/path/to/file"})
          .acl({"white list", "black list"})
          .build();

  ASSERT_EQ(tunnel->uid().name(), "my path tunnel");
  ASSERT_EQ(tunnel->type().direction(), type::kPathDirection::Client);
  ASSERT_EQ(tunnel->type().affect(), type::kPathAffect::Default);
  ASSERT_EQ(tunnel->local_point().dest(), "hostname");
  ASSERT_EQ(tunnel->local_point().host(), "hostname");  // facade
  ASSERT_EQ(tunnel->local_point().port().bound(), 12345);
  ASSERT_EQ(tunnel->remote_point().dest(), "desthash");
  ASSERT_EQ(tunnel->remote_point().port(), 31337);
  ASSERT_EQ(tunnel->hops().inbound().count(), 3);
  ASSERT_EQ(tunnel->hops().inbound().tag(), "inbound options");
  ASSERT_EQ(tunnel->hops().outbound().count(), 3);
  ASSERT_EQ(tunnel->hops().outbound().tag(), "outbound options");
  ASSERT_EQ(tunnel->crypto().data().size(), 1);
  ASSERT_EQ(tunnel->crypto().type(), crypto::kScheme::Ed25519);
  ASSERT_EQ(tunnel->crypto().file(), "/path/to/file");
  ASSERT_EQ(tunnel->acl().white_list(), "white list");
  ASSERT_EQ(tunnel->acl().black_list(), "black list");

  // Optional: in certain cases, you can alter types without needing to rebuild
  //   should you not need to reuse the originally built data
  tunnel->type({type::kPathDirection::Server, type::kPathAffect::HTTP});

  // Test that tunnel remained intact
  EXPECT_EQ(tunnel->uid().name(), "my path tunnel");
  EXPECT_EQ(tunnel->type().direction(), type::kPathDirection::Server);
  EXPECT_EQ(tunnel->type().affect(), type::kPathAffect::HTTP);
  EXPECT_EQ(tunnel->local_point().dest(), "hostname");
  ASSERT_EQ(tunnel->local_point().host(), "hostname");  // facade
  EXPECT_EQ(tunnel->local_point().port().bound(), 12345);
  // Remote point "dropped" (this is server tunnel now)
  EXPECT_EQ(tunnel->hops().inbound().count(), 3);
  EXPECT_EQ(tunnel->hops().inbound().tag(), "inbound options");
  EXPECT_EQ(tunnel->hops().outbound().count(), 3);
  EXPECT_EQ(tunnel->hops().outbound().tag(), "outbound options");
  EXPECT_EQ(tunnel->crypto().data().size(), 1);
  EXPECT_EQ(tunnel->crypto().type(), crypto::kScheme::Ed25519);
  EXPECT_EQ(tunnel->crypto().file(), "/path/to/file");
  EXPECT_EQ(tunnel->acl().white_list(), "white list");
  EXPECT_EQ(tunnel->acl().black_list(), "black list");
}

TEST_F(PathFixture, KovriTunnelBuilder)
{
  auto path = std::make_shared<type::kovri::Path>();

  std::shared_ptr<type::kovri::PathData> client =
      path->uid({"my client tunnel"})
          .type({type::kPathDirection::Client, type::kPathAffect::Default})
          .local_point({"hostname", {12345, {}}})
          .remote_point({"desthash", 31337})
          .hops({{3, "inbound options"}, {3, "outbound options"}})
          .acl({"white list", "black list"})
          .build();

  ASSERT_EQ(client->uid().name(), "my client tunnel");
  ASSERT_EQ(client->type().direction(), type::kPathDirection::Client);
  ASSERT_EQ(client->type().affect(), type::kPathAffect::Default);
  ASSERT_EQ(client->local_point().dest(), "hostname");
  ASSERT_EQ(client->local_point().port().bound(), 12345);
  ASSERT_EQ(client->remote_point().dest(), "desthash");
  ASSERT_EQ(client->remote_point().port(), 31337);
  ASSERT_EQ(client->hops().inbound().count(), 3);
  ASSERT_EQ(client->hops().inbound().tag(), "inbound options");
  ASSERT_EQ(client->hops().outbound().count(), 3);
  ASSERT_EQ(client->hops().outbound().tag(), "outbound options");
  ASSERT_EQ(client->acl().white_list(), "white list");
  ASSERT_EQ(client->acl().black_list(), "black list");

  // Create new path trait re-using parts of previous path
  auto server =
      path->uid({"my server tunnel"})
          .type({type::kPathDirection::Server, type::kPathAffect::HTTP})
          .local_point(client->local_point())
          .remote_point(client->remote_point())
          .crypto(
              {{/* no-op */}, crypto::kScheme::Ed25519 /* no-op */, "filename"})
          .acl(client->acl())
          .build();

  ASSERT_EQ(server->uid().name(), "my server tunnel");
  ASSERT_EQ(server->type().direction(), type::kPathDirection::Server);
  ASSERT_EQ(server->type().affect(), type::kPathAffect::HTTP);
  ASSERT_EQ(server->local_point().dest(), "hostname");
  ASSERT_EQ(server->local_point().port().bound(), 12345);
  // Remote point "dropped" (this is server tunnel now)
  ASSERT_EQ(server->crypto().data().size(), 0);
  ASSERT_EQ(server->crypto().type(), crypto::kScheme::Ed25519);
  ASSERT_EQ(server->crypto().file(), "filename");
  ASSERT_EQ(server->acl().white_list(), "white list");
  ASSERT_EQ(server->acl().black_list(), "black list");

  // Test that client remained intact
  EXPECT_EQ(client->uid().name(), "my client tunnel");
  EXPECT_EQ(client->type().direction(), type::kPathDirection::Client);
  EXPECT_EQ(client->type().affect(), type::kPathAffect::Default);
  EXPECT_EQ(client->local_point().dest(), "hostname");
  EXPECT_EQ(client->local_point().port().bound(), 12345);
  EXPECT_EQ(client->remote_point().dest(), "desthash");
  EXPECT_EQ(client->remote_point().port(), 31337);
  EXPECT_EQ(client->hops().inbound().count(), 3);
  EXPECT_EQ(client->hops().inbound().tag(), "inbound options");
  EXPECT_EQ(client->hops().outbound().count(), 3);
  EXPECT_EQ(client->hops().outbound().tag(), "outbound options");

  EXPECT_EQ(client->acl().white_list(), "white list");
  EXPECT_EQ(client->acl().black_list(), "black list");

  // Alter after building (pointed to data is changed)
  server->uid({"my new server tunnel"});
  server->type({type::kPathDirection::Server, type::kPathAffect::Default});
  EXPECT_EQ(server->uid().name(), "my new server tunnel");
  EXPECT_EQ(server->type().direction(), type::kPathDirection::Server);
  EXPECT_EQ(server->type().affect(), type::kPathAffect::Default);
}
