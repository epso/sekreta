// Copyright (c) 2018-2019, authors of Sekreta. See AUTHORS.md
//       or use the `git log` command on this file.
//
//    Licensed under the Sekreta License, Version 1.0,
//  which inherits from the Apache License, Version 2.0.
//   Combined, they are described as a single "License".
//
//         See the LICENSE.md file for details.

#include "api.hh"

#include <memory>
#include <string>
#include <thread>
#include <variant>

#include "gtest/gtest.h"

namespace api = sekreta::api;
namespace type = sekreta::type;

struct FactoryFixture : ::testing::Test
{
 private:
  template <typename t_derived>
  class MyCustomInterface
  {
   public:
    static constexpr type::kAPI type = type::kAPI::Socket;

   public:
    bool create() const
    {
      return static_cast<const t_derived*>(this)->create_impl();
    }
  };

  class MyCustomImpl : virtual public MyCustomInterface<MyCustomImpl>
  {
   public:
    bool create_impl() const { return true; }
    uint16_t count{};
  };

 protected:
  class MyCustomAPI final : public MyCustomImpl
  {
  };

  using t_uid = type::UID<std::string, uint32_t>;

  t_uid uid{"Jenny", 8675309};  // A common instance identifier
  api::Factory<t_uid> factory;
};

// See generic tests for implementation tests

TEST_F(FactoryFixture, CustomKey_CheckInAndOut)
{
  enum struct Custom : uint8_t
  {
    Enumeration,
  };

  Custom uid = Custom::Enumeration;
  api::Factory<Custom> factory;

  ASSERT_NO_THROW(factory.check_in<api::tor::Socket>(uid));
  ASSERT_EQ(1, factory.size());

  ASSERT_NO_THROW(factory.check_out<api::tor::Socket>(uid));
  ASSERT_EQ(0, factory.size());

  ASSERT_THROW(factory.check_out<api::tor::Socket>(uid), sekreta::Exception);
}

TEST_F(FactoryFixture, CustomKey_Create)
{
  using t_api = api::kovri::Library;
  using t_uid = std::string_view;

  api::Factory<t_uid> factory;
  t_uid uid{"I need to make you mine"};

  ASSERT_NO_THROW(factory.create<t_api>(uid));
  ASSERT_EQ(0, factory.size());

  std::shared_ptr<t_api> api = factory.create<t_api>(uid);
  ASSERT_TRUE(api);
  EXPECT_EQ(0, factory.size());

  ASSERT_THROW(factory.check_out<t_api>(uid), sekreta::Exception);
}

TEST_F(FactoryFixture, CustomAPI)
{
  using t_api = MyCustomAPI;

  std::shared_ptr<t_api> api = factory.create<t_api>(uid);
  ASSERT_TRUE(api);
  ASSERT_EQ(api->type, type::kAPI::Socket);
  ASSERT_EQ(0, factory.size());

  ASSERT_THROW(factory.check_out<t_api>(uid), sekreta::Exception);
}

TEST_F(FactoryFixture, SingleSystem_MultipleInstances)
{
  using t_key = std::string;
  using t_api = api::kovri::Library;
  using t_point = type::Point<t_key, uint16_t>;

  std::vector<std::pair<t_key, t_point>> args;
  uint16_t port{31337};

  for (uint16_t count{0}; count < 3; count++)
    args.push_back({"local instance" + std::to_string(count),
                    {"127.0.0.1", (uint16_t)(port + count)}});

  // Create instances
  api::Factory<t_key> factory;

  for (const auto& arg : args)
    factory.check_in<t_api>(arg.first);
  ASSERT_EQ(factory.size(), args.size());

  // Create params
  for (const auto& arg : args)
    {
      ASSERT_EQ(port++, arg.second.port());
      std::shared_ptr<t_api> api = factory.check_out<t_api>(arg.first);
      ASSERT_NO_THROW(
          api->configure({"--port=" + std::to_string(arg.second.port())}));
      // etc.
    }
}

TEST_F(FactoryFixture, Thread)
{
  using t_api = api::kovri::Library;
  using t_uid = size_t;

  t_uid uid{};
  api::Factory<t_uid> factory;

  auto check_in = [&](const t_uid& uid) {
    EXPECT_NO_THROW(factory.check_in<t_api>(uid));
  };

  std::thread thread([&]() {
    for (size_t i = 0; i < 100; i++)
      check_in(i);
  });

  // And also run in main thread
  for (size_t i = 100; i < 200; i++)
    check_in(i);

  thread.join();

  EXPECT_EQ(200, factory.size());

  for (size_t i = 0; i < 200; i++)
    EXPECT_EQ(true, factory.erase<t_api>(i));

  EXPECT_EQ(0, factory.size());
}

TEST_F(FactoryFixture, Scope)
{
  class Tommy
  {
   public:
    Tommy() : m_factory(std::make_shared<api::Factory<t_uid>>()) {}
    ~Tommy() = default;

   public:
    void scope(const t_uid& uid)
    {
      std::shared_ptr<MyCustomAPI> api = m_factory->create<MyCustomAPI>(uid);
      api->count = 867;
      m_factory->check_in<MyCustomAPI>(uid, api);
    }

    const auto& factory() { return m_factory; }

   private:
    std::shared_ptr<api::Factory<t_uid>> m_factory;
  };

  class Tutone
  {
   public:
    explicit Tutone(std::unique_ptr<Tommy> tommy) : m_tommy(std::move(tommy)) {}
    ~Tutone() = default;

   public:
    uint32_t scope(const t_uid& uid)
    {
      std::shared_ptr<MyCustomAPI> api =
          m_tommy->factory()->borrow<MyCustomAPI>(uid);
      return api->count + 8674442;
    }

   private:
    std::unique_ptr<Tommy> m_tommy;
  };

  using t_uid = type::UID<std::string, uint32_t>;
  const t_uid uid{"Jenny", 8675309};

  auto tommy = std::make_unique<Tommy>();
  ASSERT_NO_THROW(tommy->scope(uid));

  auto two = std::make_shared<Tutone>(std::move(tommy));
  ASSERT_EQ(two->scope(uid), uid.num());
}

// TODO(unassigned): more + better concurrency tests
// TODO: std::promise + std::future

TEST_F(FactoryFixture, KovriTunnelTrait)
{
  auto trait = factory.create<api::kovri::Trait>(uid);
  ASSERT_NE(nullptr, trait);

  auto tunnel = trait->path();
  ASSERT_NE(nullptr, tunnel);

  auto client =
      tunnel->uid({"my client tunnel"})
          .type({type::kPathDirection::Client, type::kPathAffect::Default})
          .local_point({"127.0.0.1", {12345, {}}})
          .remote_point({"desthash", 80})
          .build();
  ASSERT_NE(nullptr, client);

  EXPECT_EQ(client->uid().name(), "my client tunnel");
  EXPECT_EQ(client->type().direction(), type::kPathDirection::Client);
  EXPECT_EQ(client->type().affect(), type::kPathAffect::Default);
  EXPECT_EQ(client->local_point().dest(), "127.0.0.1");
  EXPECT_EQ(client->local_point().host(), "127.0.0.1");  // facade
  EXPECT_EQ(client->local_point().port().bound(), 12345);
  EXPECT_EQ(client->remote_point().dest(), "desthash");
  EXPECT_EQ(client->remote_point().port(), 80);

  auto server =
      tunnel->uid({"my server tunnel"})
          .type({type::kPathDirection::Server, type::kPathAffect::Default})
          .local_point({"127.0.0.1", {8080, {}}})
          .acl({"white list address", {}})
          .build();
  ASSERT_NE(nullptr, server);

  EXPECT_EQ(server->uid().name(), "my server tunnel");
  EXPECT_EQ(server->type().direction(), type::kPathDirection::Server);
  EXPECT_EQ(server->type().affect(), type::kPathAffect::Default);
  EXPECT_EQ(server->local_point().dest(), "127.0.0.1");
  EXPECT_EQ(server->local_point().port().bound(), 8080);
  EXPECT_EQ(server->acl().white_list(), "white list address");

  // Client pointer should be unaffected
  ASSERT_NE(nullptr, client);
  EXPECT_EQ(client->uid().name(), "my client tunnel");
  EXPECT_EQ(client->type().direction(), type::kPathDirection::Client);
  EXPECT_EQ(client->type().affect(), type::kPathAffect::Default);
  EXPECT_EQ(client->local_point().dest(), "127.0.0.1");
  EXPECT_EQ(client->local_point().host(), "127.0.0.1");  // facade
  EXPECT_EQ(client->local_point().port().bound(), 12345);
  EXPECT_EQ(client->remote_point().dest(), "desthash");
  EXPECT_EQ(client->remote_point().port(), 80);
}

// TODO(anonimal): profile and fix
TEST_F(FactoryFixture, KovriRunThrough)
{
  namespace api = api::kovri;
  namespace type = type::kovri;

  using t_uid = type::UID;
  sekreta::api::Factory<t_uid> factory;
  t_uid uid{"kovri", 123};

  std::shared_ptr<api::Library> lib = factory.create<api::Library>(uid);
  ASSERT_TRUE(lib);

  std::shared_ptr<api::Trait> trait = factory.create<api::Trait>(uid);
  ASSERT_TRUE(trait);

  std::shared_ptr<type::Path> path_trait = trait->path();
  ASSERT_TRUE(path_trait);

  std::shared_ptr<type::PathData> path_data =
      path_trait->uid({"my client tunnel"})
          .type({::sekreta::type::kPathDirection::Client,
                 ::sekreta::type::kPathAffect::Default})
          .local_point({"127.0.0.1", {12345, {}}})
          .remote_point({"desthash", 80})
          .build();
  ASSERT_TRUE(path_data);

  ASSERT_NO_THROW(lib->configure({"--an-argument"}));
  ASSERT_NO_THROW(lib->start());

  // TODO(anonimal): run status() in thread that polls
  std::shared_ptr<api::Library::Status> lib_status = lib->status();
  ASSERT_NO_THROW(lib_status->is_running());
  ASSERT_NO_THROW(lib_status->version());
  ASSERT_NO_THROW(lib_status->data_dir());
  ASSERT_NO_THROW(lib_status->state());
  ASSERT_NO_THROW(lib_status->uptime());
  ASSERT_NO_THROW(lib_status->path_creation_rate());
  ASSERT_NO_THROW(lib_status->path_count());
  ASSERT_NO_THROW(lib_status->active_peer_count());
  ASSERT_NO_THROW(lib_status->known_peer_count());
  ASSERT_NO_THROW(lib_status->inbound_bandwidth());
  ASSERT_NO_THROW(lib_status->outbound_bandwidth());
  ASSERT_NO_THROW(lib_status->floodfill_count());
  ASSERT_NO_THROW(lib_status->leaseset_count());

  std::shared_ptr<api::Library::Path> tunnel = lib->path(path_data);
  ASSERT_NO_THROW(tunnel->configure());
  ASSERT_NO_THROW(tunnel->start());

  // TODO(anonimal): run status() in thread that polls

  std::shared_ptr<api::Library::Path::Status> tunnel_status = tunnel->status();
  ASSERT_NO_THROW(tunnel_status->is_running());
  ASSERT_NO_THROW(tunnel_status->state());
  ASSERT_NO_THROW(tunnel_status->uptime());

  std::shared_ptr<type::UID> tunnel_uid = trait->uid();
  tunnel_uid->name({path_data->uid().name()}).num(path_data->uid().num());

  // Check-in (not terribly effective because we're still on the stack)

  // Note: UID would have to match Factory's template key
  ASSERT_NO_THROW(factory.check_in<api::Library>(uid, lib));
  ASSERT_NO_THROW(factory.check_in<api::Library::Status>(uid, lib_status));

  ASSERT_NO_THROW(factory.check_in<api::Library::Path>(*tunnel_uid, tunnel));
  ASSERT_NO_THROW(
      factory.check_in<api::Library::Path::Status>(*tunnel_uid, tunnel_status));

  ASSERT_NO_THROW(factory.check_in<api::Trait>(
      uid, trait));  // TODO(unassigned): how nice would it be to actually put
                     // saved trait data on the heap...

  // Check-out

  auto ptr_lib = factory.check_out<api::Library>(uid);
  ASSERT_TRUE(ptr_lib);

  auto ptr_lib_status = factory.check_out<api::Library::Status>(uid);
  ASSERT_TRUE(ptr_lib_status);

  auto ptr_tunnel = factory.check_out<api::Library::Path>(*tunnel_uid);
  ASSERT_TRUE(ptr_tunnel);

  auto ptr_tunnel_status =
      factory.check_out<api::Library::Path::Status>(*tunnel_uid);
  ASSERT_TRUE(ptr_tunnel_status);

  // Run no-ops

  ASSERT_NO_THROW(ptr_tunnel_status->is_running());
  ASSERT_NO_THROW(ptr_tunnel_status->state());
  ASSERT_NO_THROW(ptr_tunnel_status->uptime());

  ASSERT_NO_THROW(ptr_tunnel->stop());

  ASSERT_NO_THROW(ptr_lib_status->is_running());
  ASSERT_NO_THROW(ptr_lib_status->version());
  ASSERT_NO_THROW(ptr_lib_status->data_dir());
  ASSERT_NO_THROW(ptr_lib_status->state());
  ASSERT_NO_THROW(ptr_lib_status->uptime());
  ASSERT_NO_THROW(ptr_lib_status->path_creation_rate());
  ASSERT_NO_THROW(ptr_lib_status->path_count());
  ASSERT_NO_THROW(ptr_lib_status->active_peer_count());
  ASSERT_NO_THROW(ptr_lib_status->known_peer_count());
  ASSERT_NO_THROW(ptr_lib_status->inbound_bandwidth());
  ASSERT_NO_THROW(ptr_lib_status->outbound_bandwidth());
  ASSERT_NO_THROW(ptr_lib_status->floodfill_count());
  ASSERT_NO_THROW(ptr_lib_status->leaseset_count());

  ASSERT_NO_THROW(ptr_lib->stop());
}
