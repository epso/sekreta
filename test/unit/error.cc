// Copyright (c) 2018-2019, authors of Sekreta. See AUTHORS.md
//       or use the `git log` command on this file.
//
//    Licensed under the Sekreta License, Version 1.0,
//  which inherits from the Apache License, Version 2.0.
//   Combined, they are described as a single "License".
//
//         See the LICENSE.md file for details.

#include "gtest/gtest.h"

#include "error.hh"

struct ExceptionFixture : ::testing::Test
{
 protected:
  std::string what = "message";
  using t_ex = sekreta::Exception::kType;
};

TEST_F(ExceptionFixture, RuntimeError)
{
  try
    {
      throw sekreta::exception::RuntimeError(what);
    }
  catch (const sekreta::Exception& ex)
    {
      EXPECT_EQ(ex.type(), t_ex::RuntimeError);
      EXPECT_EQ(ex.what(), what);
    }
}

TEST_F(ExceptionFixture, InvalidArgument)
{
  try
    {
      throw sekreta::exception::InvalidArgument(what);
    }
  catch (const sekreta::Exception& ex)
    {
      EXPECT_EQ(ex.type(), t_ex::InvalidArgument);
      EXPECT_EQ(ex.what(), what);
    }
}

TEST_F(ExceptionFixture, InternalError)
{
  try
    {
      throw sekreta::exception::InternalError(what);
    }
  catch (const sekreta::Exception& ex)
    {
      EXPECT_EQ(ex.type(), t_ex::InternalError);
      EXPECT_EQ(ex.what(), what);
    }
}

TEST_F(ExceptionFixture, NotImplemented)
{
  try
    {
      throw sekreta::exception::NotImplemented(what);
    }
  catch (const sekreta::Exception& ex)
    {
      EXPECT_EQ(ex.type(), t_ex::NotImplemented);
      EXPECT_EQ(ex.what(), what);
    }
}

TEST_F(ExceptionFixture, SEK_THROW)
{
  try
    {
      SEK_THROW(sekreta::exception::RuntimeError, message);
    }
  catch (const sekreta::Exception& ex)
    {
      EXPECT_EQ(ex.type(), t_ex::RuntimeError);
    }
}

TEST_F(ExceptionFixture, SEK_THROW_IF)
{
  try
    {
      SEK_THROW_IF(false, sekreta::exception::RuntimeError, message);
    }
  catch (const sekreta::Exception& ex)
    {
      EXPECT_EQ(ex.type(), t_ex::RuntimeError);
    }
}

TEST_F(ExceptionFixture, SEK_THROW_ASSERT)
{
  try
    {
      SEK_THROW_ASSERT(sekreta::exception::RuntimeError, message);
    }
  catch (const sekreta::Exception& ex)
    {
      EXPECT_EQ(ex.type(), t_ex::RuntimeError);
    }
}

TEST_F(ExceptionFixture, SEK_THROW_ASSERT_IF)
{
  try
    {
      SEK_THROW_ASSERT_IF(false, sekreta::exception::RuntimeError, message);
    }
  catch (const sekreta::Exception& ex)
    {
      EXPECT_EQ(ex.type(), t_ex::RuntimeError);
    }
}
