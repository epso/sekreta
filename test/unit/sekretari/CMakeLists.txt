# Copyright (c) 2018-2019, authors of Sekreta. See AUTHORS.md
#    or use the `git log` command on this file to review
#        an individual's licensed/attributed work.
#
#    Licensed under the Sekreta License, Version 1.0,
#  which inherits from the Apache License, Version 2.0.
#   Combined, they are described as a single "License".
#
#         See the LICENSE.md file for details.

cmake_minimum_required(VERSION 3.10.2...3.16.2)

# TODO(unassigned): foreach isn't very bright with actual lists?...
# TODO(unassigned): yes, awkward, but this is needed for 3.10.2 unless we build for an additional binary

set(4se "4se.cc")
set(did "did.cc")
set(eds "eds.cc")
set(sek "sek.cc")
set(ssd "ssd.cc")
foreach(file IN LISTS
    4se
    did
    eds
    sek
    ssd)
  target_sources(${UNIT_TESTS} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/${file})
endforeach()
