// Copyright (c) 2018-2019, authors of Sekreta. See AUTHORS.md
//    or use the `git log` command on this file to review
//        an individual's licensed/attributed work.
//
//    Licensed under the Sekreta License, Version 1.0,
//  which inherits from the Apache License, Version 2.0.
//   Combined, they are described as a single "License".
//
//         See the LICENSE.md file for details.

#include "sekretari/sek.hh"

#include "gtest/gtest.h"

// TODO(anonimal): finish WIP branch
