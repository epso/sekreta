// Copyright (c) 2018-2019, authors of Sekreta. See AUTHORS.md
//       or use the `git log` command on this file.
//
//    Licensed under the Sekreta License, Version 1.0,
//  which inherits from the Apache License, Version 2.0.
//   Combined, they are described as a single "License".
//
//         See the LICENSE.md file for details.

#include "type.hh"

#include <benchmark/benchmark.h>

namespace type = sekreta::type;
namespace crypto = type::crypto;

struct Type : public benchmark::Fixture
{
  using t_uid = type::UID<std::string_view, uint32_t>;
  using t_port = type::Port<uint16_t, uint16_t>;
  using t_point = type::Point<std::string_view, t_port>;
  using t_acl = type::ACL<std::string_view, std::string_view>;
  using t_path_type = type::PathType<type::kPathDirection, type::kPathAffect>;
  using t_path = type::Path<>;
  using t_hops_type = type::HopsType<uint8_t, std::string_view>;
  using t_hops = type::Hops<t_hops_type>;

  t_uid uid{};
  t_port port{};
  t_point point{};
  t_acl acl{};
  t_path_type path_type{};
  t_path path{};
  t_hops_type hops_type{};
  t_hops hops{};
};

// TODO(anonimal): concurrency

BENCHMARK_F(Type, UID_Mutators)
(benchmark::State& state)
{
  for (auto _ : state)
    uid.name("uid").num(123);
}

BENCHMARK_F(Type, UID_Accessors)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      uid.name();
      uid.num();
    }
}

BENCHMARK_F(Type, Port_Mutators)
(benchmark::State& state)
{
  for (auto _ : state)
    port.bound(12345).unbound(54321);
}

BENCHMARK_F(Type, Port_Accessors)
(benchmark::State& state)
{
  port.bound(12345).unbound(54321);

  for (auto _ : state)
    {
      port.bound();
      port.unbound();
    }
}

BENCHMARK_F(Type, Point_Mutators)
(benchmark::State& state)
{
  for (auto _ : state)
    point.dest("127.0.0.1").port({12345, 54321});
}

BENCHMARK_F(Type, Point_Accessors)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      point.dest();
      point.port().bound();
      point.port().unbound();
    }
}

BENCHMARK_F(Type, ACL_Mutators)
(benchmark::State& state)
{
  for (auto _ : state)
    acl.white_list("127.0.0.1").black_list("0.0.0.0");
}

BENCHMARK_F(Type, ACL_Accessors)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      acl.white_list();
      acl.black_list();
    }
}

BENCHMARK_F(Type, PathType_Mutators)
(benchmark::State& state)
{
  for (auto _ : state)
    path_type.direction(type::kPathDirection::Client)
        .affect(type::kPathAffect::HTTP);
}

BENCHMARK_F(Type, PathType_Accessors)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      path_type.direction();
      path_type.affect();
    }
}

BENCHMARK_F(Type, HopsType_Mutators)
(benchmark::State& state)
{
  for (auto _ : state)
    hops_type.count(3).tag({"tag"});
}

BENCHMARK_F(Type, HopsType_Accessors)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      hops_type.count();
      hops_type.tag();
    }
}

BENCHMARK_F(Type, Hops_Mutators)
(benchmark::State& state)
{
  for (auto _ : state)
    hops.inbound({3, "itag"}).outbound({3, "otag"});
}

BENCHMARK_F(Type, Hops_Accessors)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      hops.inbound();
      hops.inbound().count();
      hops.inbound().tag();

      hops.outbound();
      hops.outbound().count();
      hops.outbound().tag();
    }
}

BENCHMARK_F(Type, Path_Mutators)
(benchmark::State& state)
{
  auto path = std::make_shared<t_path>();
  for (auto _ : state)
    {
      path->uid({"my custom path"})
          .type({type::kPathDirection::Client, type::kPathAffect::Default})
          .local_point({"hostname", {12345, {}}})
          .remote_point({"desthash", 31337})
          .hops({{3, "inbound options"}, {3, "outbound options"}})
          .crypto(
              {{std::byte{0x00}}, crypto::kScheme::Ed25519, "/path/to/file"})
          .acl({"white list", "black list"})
          .build();
    }
}

BENCHMARK_F(Type, Path_Accessors)
(benchmark::State& state)
{
  auto path = std::make_shared<t_path>();

  auto custom =
      path->uid({"my custom path"})
          .type({type::kPathDirection::Client, type::kPathAffect::Default})
          .local_point({"hostname", {12345, {}}})
          .remote_point({"desthash", 31337})
          .hops({{3, "inbound options"}, {3, "outbound options"}})
          .crypto(
              {{std::byte{0x00}}, crypto::kScheme::Ed25519, "/path/to/file"})
          .acl({"white list", "black list"})
          .build();

  for (auto _ : state)
    {
      custom->uid().name();

      custom->type().direction();
      custom->type().affect();

      custom->local_point().dest();
      custom->local_point().port().bound();

      custom->remote_point().dest();
      custom->remote_point().port();

      custom->hops().inbound();
      custom->hops().inbound().count();
      custom->hops().inbound().tag();

      custom->hops().outbound();
      custom->hops().outbound().count();
      custom->hops().outbound().tag();

      custom->crypto().data().size();
      custom->crypto().type();
      custom->crypto().file();

      custom->acl().white_list();
      custom->acl().black_list();
    }
}

struct CryptoType : public benchmark::Fixture
{
  using t_data = std::vector<std::byte>;
  using t_key_pair = crypto::KeyPair<t_data>;
  using t_key_tuple =
      crypto::Key<std::tuple<t_data, crypto::kScheme, std::string_view>>;
  using t_key_super =
      crypto::SuperKey<t_data, crypto::kScheme, std::string_view>;

  t_key_pair key_pair{};
  t_key_tuple key_tuple{};
  t_key_super key_super{};
};

BENCHMARK_F(CryptoType, KeyPair_Mutators)
(benchmark::State& state)
{
  for (auto _ : state)
    key_pair.pk({std::byte{0x00}}).sk({std::byte{0x01}});
}

BENCHMARK_F(CryptoType, KeyPair_Accessors)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      key_pair.pk();
      key_pair.sk();
    }
}

BENCHMARK_F(CryptoType, KeyTuple_Mutators)
(benchmark::State& state)
{
  for (auto _ : state)
    key_tuple.data({std::byte{0x00}})
        .type(type::crypto::kScheme::DH)
        .file("/path");
}

BENCHMARK_F(CryptoType, KeyTuple_Accessors)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      key_tuple.data();
      key_tuple.type();
      key_tuple.file();
    }
}

BENCHMARK_F(CryptoType, KeySuper_Mutators)
(benchmark::State& state)
{
  for (auto _ : state)
    key_super.crypto({{std::byte{0x00}}, type::crypto::kScheme::DH, "/path"})
        .meta("metadata");
}

BENCHMARK_F(CryptoType, KeySuper_Accessors)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      key_super.crypto().data();
      key_super.crypto().type();
      key_super.crypto().file();
      key_super.meta();
    }
}

// TODO(anonimal): more benchmarks
