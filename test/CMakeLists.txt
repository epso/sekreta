# Copyright (c) 2018-2019, authors of Sekreta. See AUTHORS.md
#       or use the `git log` command on this file.
#
#    Licensed under the Sekreta License, Version 1.0,
#  which inherits from the Apache License, Version 2.0.
#   Combined, they are described as a single "License".
#
#         See the LICENSE.md file for details.

cmake_minimum_required(VERSION 3.10.2...3.16.2)

if (BUILD_TEST)
  include(GoogleTest)
  add_subdirectory(unit)
endif()

if (BUILD_BENCHMARK)
  add_subdirectory(benchmark)
endif()
