Copyright (c) 2018-2019, authors of Sekreta. See AUTHORS.md.

Licensed under the Sekreta License, Version 1.0, which inherits from the
Apache License, Version 2.0. Combined, they are described as a single "License";

A Code of Conduct is defined as a document of discriminatory rhetoric against
any contributor or licensor in which the subjective basis of their human
interaction between other contributors or licensors during the course of source
code development or intellectual property development will be judged and
unilaterally dismissed or deemed inappropriate and subsequently used as leverage
to disqualify said contributor or licensor or their contribution based on the
subjective quality of said interaction.

The Sekreta License requires that a Code of Conduct never be written, nor
implemented, nor enforced within, nor upon, the Sekreta source code, the
contributors to Sekreta, contributions to Sekreta, nor on the Sekreta project
itself, nor within nor upon any forks of the Sekreta source code or Sekreta
project.

Any project or software that uses Sekreta is free to apply their own Code of
Conduct to their source code or project but must not retroactively enforce said
conduct upon Sekreta licensors or contributors on the basis of their Sekreta
contributions or intellectual property.

This License is not a dual license. You may not use this file or any other file
or any other licensed Sekreta intellectual property if not in compliance with
the License. Remaining license usage and distribution mandates follow the
guidelines outline by the Apache License. For the remainder of this License,
you may obtain a copy of the Apache License at:

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

----

The Sekreta logo is licensed under the [CC BY-SA 4.0 license](https://creativecommons.org/licenses/by-sa/4.0/).

Copyright (c) 2018-2019, anonimal <anonimal@sekreta.org>

----

All logos and images in the `img` directory are used under their respective licenses. See their websites for details.
