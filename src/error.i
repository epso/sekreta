//! \file
//!
//! \brief SWIG interface file to Sekreta's error handling
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright Sekreta License, Version 1.0. See the LICENSE.md file for details
//!
//! \todo Make it so.

%module pysek_error
%{ #include "error.hh" %}
