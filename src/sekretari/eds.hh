//! \file sekretari/eds.hh
//!
//! \brief Implements the Sekretari EDS component
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright Sekreta License, Version 1.0. See the LICENSE.md file for details
//!
//! \todo Merge WIP branch when in working condition

#ifndef SRC_SEKRETARI_EDS_HH_
#define SRC_SEKRETARI_EDS_HH_

#endif  // SRC_SEKRETARI_EDS_HH_
