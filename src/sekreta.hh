//! \file sekreta.hh
//!
//! \brief All-in-one convenience include for super-projects
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright Sekreta License, Version 1.0. See the LICENSE.md file for details

#ifndef SRC_SEKRETA_HH_
#define SRC_SEKRETA_HH_

#include "api.hh"
#include "error.hh"
#include "generic.hh"
#include "impl_helper.hh"
#include "sekretari.hh"
#include "system.hh"
#include "type.hh"

#endif  // SRC_SEKRETA_HH_
