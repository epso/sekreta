//! \file api.hh
//!
//! \brief Sekreta's public APIs for all underlying anonymity systems
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright Sekreta License, Version 1.0. See the LICENSE.md file for details
//!
//! \details Sekreta's APIs are a set of highly abstracted classes which hide
//!   all internal implementation details; including the Sekretari subsystem and
//!   all underlying anonymity systems' APIs. Like a sliding glass window, the
//!   Sekreta APIs offer a sense of transparency into the underlying system APIs
//!   while allowing a uniform framework to work with. This unified style allows
//!   the same idioms to be used across all systems: so long as you know which
//!   type of API you wish to use, you can expect that every namespace and
//!   nested class will remain the "same" across all systems.
//!
//!   When confronted with the problem of creating unified interfaces across
//!   many underlying systems (all of which require many different types), the
//!   current Sekreta solution is to utilize template meta programming
//!   techniques along with static polymorphism. This provides benefits such as:
//!
//!     - Eliminating the voracious use of the vtable
//!     - Prevents the need to translate interface-provided types into
//!       implementation-required types (because dynamic polymorphism does not
//!       play well when working with multiple types upon the same interface)
//!     - Abstracts the implementation to such a degree that you can utilize a
//!       testing framework without the need for any underlying system
//!       implementation
//!     - Function templates allow super-projects to create custom system
//!       implementations without the need to refactor public APIs. This
//!       prevents super-projects from breaking interfaces for others whiling
//!       still giving them the flexibility they desire
//!
//!   In all, the API interfaces hook into generic internal interfaces which in
//!   turn provide the glue that binds the implementation.
//!
//! \todo The API may actually benefit more from a more restrictive type system.
//!
//! \warning API users should use the API factory/pool instead of instantiating
//!   API classes directly. Doing so will help you get off the stack onto the
//!   heap and will also allow you to make use of the *Sekretari* subsystem.
//!
//! \note The policy of API function should tend to be: keep function parameters
//!   as an *in*-only policy with return types being an *out*-only policy. Also,
//!   notice that the lack of function parameters (until needed). This allows
//!   future-proofing for additional hooks and other internals while maintaining
//!   clean and reliable interfaces.
//!
//! \note Concurrency is a WIP so you should be considerate of how you use API
//!   objects and how you fire off functions into threads. Ultimately, two or
//!   more threads *should* be able to invoke a function concurrently on the
//!   same object and it *should* be safe for two or more threads to invoke a
//!   concurrently on the same object.
//!
//! \note You'll notice the strategic use of shared_ptr. Copies are intentional
//!   and you should (eventually) share freely across threads. `const &`
//!   parameters are also intentional in that they indicate the callee will not
//!   re-seat nor take full ownership. Passing shared_ptrs in this manner is
//!   also a means of unifying the interfaces (the cost incurred is marginal
//!   because of current usage-requirements (no tight loops, etc.)).
//!
//! \todo ASIO callback hooks for Kohlhoff's stand-alone ASIO

#ifndef SRC_API_HH_
#define SRC_API_HH_

#include <any>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "system.hh"
#include "type.hh"

//! \brief Sekreta's public APIs for public consumption
namespace sekreta::api
{
//! \brief API factory/pool used for all public APIs
//!
//! \details APIs can be free-flowing and mixed with each other within this
//!   factory. Any managing/handling is done internally.
//!
//! \tparam t_uid UID key type for associated API
template <typename t_uid>
class Factory
    : virtual public ::sekreta::internal::api::
          AbstractFactory<t_uid, ::sekreta::type::kAPI, std::any, std::multimap>
{
};

//! \brief Public APIs for this anonymity system *only*
namespace kovri
{
//! \brief Convenience alias, for internal use only. Not for public consumption.
namespace internal = ::sekreta::internal::api::kovri;

//! \brief Kovri library API
//! \details API interface for a single library instance
//! \todo Many more methods can be added
class Library final : public internal::Library
{
  //! \brief Internal alias for base-type
  using t_library = internal::Library;

 public:
  Library() : t_library(std::make_shared<internal::impl_wrapper::Library>()) {}

 public:
  //! \brief Interface for library instance configuration
  //! \param arg Implementation-defined arguments for configuration
  template <
      typename t_ret = void,
      typename t_arg = const std::vector<std::string>&>
  t_ret configure(t_arg arg)
  {
    return t_library::template configure<t_ret, t_arg>(arg);
  }

  //! \brief Interface for library instance start
  template <typename t_ret = void>
  t_ret start()
  {
    return t_library::template start<t_ret>();
  }

  //! \brief Interface for library instance restart
  template <typename t_ret = void>
  t_ret restart()
  {
    return t_library::template restart<t_ret>();
  }

  //! \brief Interface for library instance stop
  template <typename t_ret = void>
  t_ret stop()
  {
    return t_library::template stop<t_ret>();
  }

 public:
  //! \brief Library status interface
  //! \details For this API, induces a router's status
  //! \todo More functions for info that's log printed (listening port, etc.)
  //! \todo More functions for features provided by other anonymity system APIs
  //! \todo Mutators for underlying systems should be interfaced here
  class Status final : public t_library::Status
  {
    //! \brief Internal alias for base-type
    using t_status = t_library::Status;

   public:
    //! \brief Construct with pointer to shared library status wrapper
    explicit Status(
        const std::shared_ptr<internal::impl_wrapper::Library::Status>& status)
        : t_status(status)
    {
    }

   public:
    //! \brief Interface to library instance router check if running
    //! \return True if running, false if not running
    template <typename t_ret = bool>
    t_ret is_running()
    {
      return t_status::template is_running<t_ret>();
    }

    //! \brief Interface to library instance system version
    //! \return Kovri version
    //! \todo Expand (version num, version name, version commit, etc.)
    template <typename t_ret = std::string>
    t_ret version()
    {
      return t_status::template version<t_ret>();
    }

    //! \brief Interface to library instance data directory
    //! \return Kovri data directory
    template <typename t_ret = std::string>
    t_ret data_dir()
    {
      return t_status::template data_dir<t_ret>();
    }

    //! \brief Interface to library instance router state
    //! \return Kovri state
    template <typename t_ret = std::string>
    t_ret state()
    {
      return t_status::template state<t_ret>();
    }

    //! \brief Interface to library instance router uptime
    //! \return Kovri uptime in seconds
    template <typename t_ret = uint64_t>
    t_ret uptime()
    {
      return t_status::template uptime<t_ret>();
    }

    //! \brief Interface to library instance router tunnel creation rate
    //! \return Kovri router tunnel creation rate in percentage (0-100%)
    template <typename t_ret = size_t>
    t_ret path_creation_rate()
    {
      return t_status::template path_creation_rate<t_ret>();
    }

    //! \brief Interface to library instance router tunnel count
    //! \return Kovri router active tunnel count
    template <typename t_ret = size_t>
    t_ret path_count()
    {
      return t_status::template path_count<t_ret>();
    }

    //! \brief Interface to library instance router active peer count
    //! \return Kovri active peer count
    template <typename t_ret = size_t>
    t_ret active_peer_count()
    {
      return t_status::template active_peer_count<t_ret>();
    }

    //! \brief Interface to library instance router known peer count
    //! \return Kovri known peer count
    template <typename t_ret = size_t>
    t_ret known_peer_count()
    {
      return t_status::template known_peer_count<t_ret>();
    }

    //! \brief Interface to library instance router inbound bandwidth in-use
    //! \return Kovri inbound bandwidth in bytes
    template <typename t_ret = uint32_t>
    t_ret inbound_bandwidth()
    {
      return t_status::template inbound_bandwidth<t_ret>();
    }

    //! \brief Interface to library instance router inbound bandwidth in-use
    //! \return Kovri outbound bandwidth in bytes
    template <typename t_ret = uint32_t>
    t_ret outbound_bandwidth()
    {
      return t_status::template outbound_bandwidth<t_ret>();
    }

    //! \brief Interface to library instance router floodfill count
    //! \return Kovri floodfill count
    template <typename t_ret = size_t>
    t_ret floodfill_count()
    {
      return t_status::template floodfill_count<t_ret>();
    }

    //! \brief Interface to library instance router leaseset count
    //! \return Kovri leaseset count
    template <typename t_ret = size_t>
    t_ret leaseset_count()
    {
      return t_status::template leaseset_count<t_ret>();
    }
  };

  //! \brief Creates a library status API interface
  //! \return Pointer to shared library status interface
  template <typename t_ret = std::shared_ptr<Library::Status>>
  t_ret status()
  {
    return t_library::template status<t_ret>();
  }

  //! \brief Library path interface
  //! \note This is a *client* path interface, not a *router* path
  class Path final : public t_library::Path
  {
    //! \brief Internal alias for base-type
    using t_path = t_library::Path;

   public:
    //! \throws sekreta::Exception on nullptr
    explicit Path(
        const std::shared_ptr<internal::impl_wrapper::Library::Path>& path)
        : t_path(path)
    {
    }

   public:
    //! \brief Interface for library client tunnel configuration
    template <typename t_ret = void>
    t_ret configure()
    {
      return t_path::template configure<t_ret>();
    }

    //! \brief Interface for library client tunnel start
    template <typename t_ret = void>
    t_ret start()
    {
      return t_path::template start<t_ret>();
    }

    //! \brief Interface for library client tunnel restart
    template <typename t_ret = void>
    t_ret restart()
    {
      return t_path::template restart<t_ret>();
    }

    //! \brief Interface for library client tunnel stop
    template <typename t_ret = void>
    t_ret stop()
    {
      return t_path::template stop<t_ret>();
    }

    //! \brief Library path status interface
    class Status final : public t_library::Path::Status
    {
      //! \brief Internal alias for base-type
      using t_status = t_library::Path::Status;

     public:
      //! \throws sekreta::Exception on nullptr
      explicit Status(
          const std::shared_ptr<internal::impl_wrapper::Library::Path::Status>&
              status)
          : t_status(status)
      {
      }

     public:
      //! \brief Interface to library instance client tunnel check if running
      //! \return True if running, false if not running
      template <typename t_ret = bool>
      t_ret is_running()
      {
        return t_status::template is_running<t_ret>();
      }

      //! \brief Interface to library instance client tunnel uptime
      //! \return Kovri client tunnel uptime
      template <typename t_ret = uint64_t>
      t_ret uptime()
      {
        return t_status::template uptime<t_ret>();
      }

      //! \brief Interface to library instance client tunnel state
      //! \return Kovri client tunnel state
      template <typename t_ret = std::string>
      t_ret state()
      {
        return t_status::template state<t_ret>();
      }
    };

    //! \brief Creates library path status interface
    //! \details Does not hook directly into system implementation but rather
    //!   builds reference pointer to status interface (which hooks into
    //!   implementation) while using mutex from glue interface
    //! \return Pointer to shared library path status interface
    template <typename t_ret = std::shared_ptr<Library::Path::Status>>
    t_ret status()
    {
      return t_path::template status<t_ret>();
    }
  };

  //! \brief Creates library path interface
  //! \param arg Sekreta *built* path data
  //! \note Allowing the path data argument here allows the following:
  //!   - Optional impl-specific arguments can be cleanly passed to Path
  //!     interface functions
  //!   - Extensibility: you can alter the Path impl within the wrapper, add as
  //!     many arguments as you'd like, and only ever have to change a single
  //!     line when initializing the wrapper (internally)
  //! \return Pointer to shared library path interface
  //! \todo ASIO callback
  template <
      typename t_ret = std::shared_ptr<Library::Path>,
      typename t_arg = std::shared_ptr<::sekreta::type::kovri::PathData>>
  t_ret path(t_arg arg)
  {
    return t_library::template path<t_ret, t_arg>(arg);
  }
};

//! \todo Finish and merge branch
//! \todo ASIO callback
class Socket final : public internal::Socket
{
};

//! \todo Finish and merge branch
class Plugin final : public internal::Plugin
{
};

//! \brief Trait API
//! \details Allows uniformity when interfacing with various Sekreta components
//!   Traits produced are *guaranteed* to be the types required to interface
//!   with respective system APIs and components
//! \todo How nice would it be to actually put saved trait data on the heap...
class Trait final : public internal::Trait
{
  //! \brief Internal alias for base-type
  using t_trait = internal::Trait;

 public:
  //! \brief Is a Trait type
  static constexpr ::sekreta::type::kAPI type = ::sekreta::type::kAPI::Trait;

  //! \brief Is a Kovri system
  static constexpr ::sekreta::type::kSystem system =
      ::sekreta::type::kSystem::Kovri;

 private:
  //! \brief Internal alias for UID-type
  using t_uid = ::sekreta::type::kovri::UID;

  //! \brief Internal alias for Path-type
  using t_path = ::sekreta::type::kovri::Path;

 public:
  //! \brief Interface to system UID trait
  //! \return Pointer to shared trait for use by Kovri UIDs
  template <typename t_ret = std::shared_ptr<t_uid>>
  t_ret uid()
  {
    return t_trait::template uid<t_ret>();
  }

  //! \brief Interface to system path trait
  //! \details Used when building paths
  //! \return Pointer to shared trait for use by Kovri tunnels
  template <typename t_ret = std::shared_ptr<t_path>>
  t_ret path()
  {
    return t_trait::template path<t_ret>();
  }
};

}  // namespace kovri

//! \brief Public APIs for this anonymity system *only*
//! \todo Finish and merge branch
namespace ire
{
//! \brief Convenience alias, for internal use only. Not for public consumption.
namespace internal = ::sekreta::internal::api::ire;

//! \todo FFI hooks
class Library final : public internal::Library
{
};
class Socket final : public internal::Socket
{
};
class Plugin final : public internal::Plugin
{
};
class Trait final : public internal::Trait
{
};
}  // namespace ire

//! \brief Public APIs for this anonymity system *only*
//! \todo Finish and merge branch
namespace i2p
{
//! \brief Convenience alias, for internal use only. Not for public consumption.
namespace internal = ::sekreta::internal::api::ire;

class Library final : public internal::Library
{
};
class Socket final : public internal::Socket
{
};
class Plugin final : public internal::Plugin
{
};
class Trait final : public internal::Trait
{
};
}  // namespace i2p

//! \brief Public APIs for this anonymity system *only*
//! \todo Finish and merge branch
namespace tor
{
//! \brief Convenience alias, for internal use only. Not for public consumption.
namespace internal = ::sekreta::internal::api::ire;

class Library final : public internal::Library
{
};
class Socket final : public internal::Socket
{
};
class Plugin final : public internal::Plugin
{
};
class Trait final : public internal::Trait
{
};
}  // namespace tor

//! \brief Public APIs for this anonymity system *only*
//! \todo Finish and merge branch
namespace loki
{
//! \brief Convenience alias, for internal use only. Not for public consumption.
namespace internal = ::sekreta::internal::api::ire;

class Library final : public internal::Library
{
};
class Socket final : public internal::Socket
{
};
class Plugin final : public internal::Plugin
{
};
class Trait final : public internal::Trait
{
};
}  // namespace loki

}  // namespace sekreta::api

#endif  // SRC_API_HH_
