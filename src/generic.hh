//! \file generic.hh
//!
//! \brief Generic code used throughout the entire Sekreta(ri) system
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright Sekreta License, Version 1.0. See the LICENSE.md file for details
//!
//! \details Employs highly reusable code with an emphasis on TMP abstractions.
//!   Such abstraction allows seamless additions to this "glue" code between
//!   interfaces and implementations (logging, mutex handling, custom code;
//!   anything that your heart desires).
//!
//! \note Concrete implementations should never go in this file

#ifndef SRC_GENERIC_HH_
#define SRC_GENERIC_HH_

#include <any>
#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <optional>
#include <string>
#include <utility>
#include <variant>

#include "error.hh"
#include "type.hh"

//! \brief All internal Sekreta components. Not for *direct* public consumption
namespace sekreta::internal
{
//! \brief Sekreta-type-friendly comparator
template <typename t_key>
struct Comparator
{
  //! \brief Comparator's functor for Sekreta types or standard types
  //! \todo We really want *all* UID types
  bool operator()(const t_key& lhs, const t_key& rhs) const
  {
    if constexpr (
        std::is_base_of_v<
            sekreta::type::UID<std::string_view>,
            t_key> || std::is_base_of_v<sekreta::type::UID<std::string>, t_key>)
      {
        if (lhs.name() == rhs.name())
          return lhs.num() > rhs.num();
        return lhs.name() < rhs.name();
      }
    else
      {
        std::less<t_key> less;
        return less(lhs, rhs);
      }
  }
};

//! \brief Unique abstract factory template
//! \tparam t_key Key to store factory with
//! \tparam t_factory Factory/factories to manage
//! \tparam t_container Container type
template <
    typename t_key,
    typename t_factory,
    template <typename...>
    typename t_container>
class AbstractFactory
{
};

//! \brief Generic abstract variant factory/pool, specialized with std::map
//!
//! \warning Factory type must be a variant object (std::variant) containing
//!   factory objects
//!
//! \note Factory objects are move only
//!
//! \todo Refactor to allow parameter pack factories, keep
//!   variant within implementation only
//!
//! \todo Don't force reference for types that benefit from copy (string_view,
//!  integral, trivial, etc.)
//!
//! \todo Consider enforcing clone over copying
template <typename t_key, typename t_factory>
class AbstractFactory<t_key, t_factory, std::map>
{
  //! \brief Internal alias for container-type
  using t_container =
      std::map<t_key, std::unique_ptr<t_factory>, Comparator<t_key>>;

 protected:
  AbstractFactory() = default;
  ~AbstractFactory() = default;

  //! \brief Default move-ctor
  AbstractFactory(AbstractFactory&&) = default;

  //! \brief Default move-assignment
  AbstractFactory& operator=(AbstractFactory&& factory) = default;

  //! \brief Thread-safe copy-ctor
  //! \note Privately implemented
  AbstractFactory(const AbstractFactory& factory)
      : AbstractFactory(factory, std::scoped_lock<std::mutex>(factory.m_mutex))
  {
  }

  //! \brief Thread-safe copy-assignment
  AbstractFactory& operator=(const AbstractFactory& factory)
  {
    std::scoped_lock lock(m_mutex, factory.m_mutex);
    m_container.clear();
    for (const auto& it : factory.m_container)
      m_container.insert({it.first, std::make_unique<t_factory>(*it.second)});
    return *this;
  }

 private:
  //! \brief Thread-safe copy-ctor implementation
  AbstractFactory(
      const AbstractFactory& factory,
      const std::scoped_lock<std::mutex>&)
  {
    for (const auto& it : factory.m_container)
      m_container.insert({it.first, std::make_unique<t_factory>(*it.second)});
  }

 public:
  //! \brief Create and return factory instance
  //! \tparam t_index Factory instance object
  //! \param key Key mapped to instance
  //! \return Pointer to created instance
  //! \warning Created instances will not throw if created more than once with
  //!   same key
  template <typename t_index>
  std::unique_ptr<t_index> create(const t_key& key)
  {
    std::scoped_lock lock(m_mutex);
    insert<t_index>(key);
    return extract<t_index>(key);
  }

  //! \brief Check-in a factory instance
  //! \details Checked-in instances are stored for later use
  //! \tparam t_index Factory instance object
  //! \param key Key mapped to instance
  //! \throws sekreta::Exception
  template <typename t_index>
  void check_in(const t_key& key)
  {
    std::scoped_lock lock(m_mutex);
    insert<t_index>(key);
  }

  //! \brief Check-out a factory instance
  //! \tparam t_index Factory instance object
  //! \param key Key mapped to instance
  //! \return Pointer to created instance
  //! \throws sekreta::Exception
  //! \warning Checked-out instances are no longer stored within the container
  template <typename t_index>
  std::unique_ptr<t_index> check_out(const t_key& key)
  {
    std::scoped_lock lock(m_mutex);
    return extract<t_index>(key);
  }

  //! \brief Erase factory instance
  //! \tparam t_index Factory instance object
  //! \param key Key mapped to instance
  //! \return True if instance was erased
  size_t erase(const t_key& key)
  {
    std::scoped_lock lock(m_mutex);
    return m_container.erase(key);
  }

  //! \brief Clear the container of all factory objects
  void clear()
  {
    std::scoped_lock lock(m_mutex);
    m_container.clear();
  }

  //! \return Size of the container (count of all the checked-in factory
  //!   objects)
  size_t size() const
  {
    std::scoped_lock lock(m_mutex);
    return m_container.size();
  }

  //! \brief Immutable accessor to container
  const auto& operator()() const noexcept { return m_container; }

 private:
  //! \brief Insert factory into the abstract factory
  template <typename t_index>
  void insert(const t_key& key)
  {
    m_container.insert_or_assign(key, std::make_unique<t_factory>(t_index{}));
  }

  //! \brief Extract factory from the abstract factory
  template <typename t_index>
  std::unique_ptr<t_index> extract(const t_key& key)
  {
    std::unique_ptr<t_index> factory;

    auto node = m_container.extract(key);
    if (!node.empty())
      factory = std::make_unique<t_index>(std::get<t_index>(*node.mapped()));

    SEK_THROW_IF(!factory, exception::RuntimeError, "Factory unavailable");
    return factory;
  }

 protected:
  //! \brief Base-type mutex
  //! \note May be shared by a derived class
  mutable std::mutex m_mutex;

 private:
  t_container m_container;
};

//! \brief Generic abstract variant factory/pool, specialized with std::multimap
//!
//! \warning Factory type must be a variant object (std::variant) containing
//!   factory objects
//!
//! \note Factory objects are move only
//!
//! \todo Refactor to allow parameter pack factories, keep
//!   variant within implementation only
//!
//! \todo Don't force reference for types that benefit from copy (string_view,
//!   integral, trivial, etc.)
//!
//! \todo Consider enforcing clone over copying
template <typename t_key, typename t_factory>
class AbstractFactory<t_key, t_factory, std::multimap>
{
  //! \brief Internal alias for container-type
  using t_container =
      std::multimap<t_key, std::unique_ptr<t_factory>, Comparator<t_key>>;

 protected:
  AbstractFactory() = default;
  ~AbstractFactory() = default;

  //! \brief Default move-ctor
  AbstractFactory(AbstractFactory&&) = default;

  //! \brief Default move-assignment
  AbstractFactory& operator=(AbstractFactory&&) = default;

  //! \brief Thread-safe copy-ctor
  //! \note Privately implemented
  AbstractFactory(const AbstractFactory& factory)
      : AbstractFactory(factory, std::scoped_lock<std::mutex>(factory.m_mutex))
  {
  }

  //! \brief Thread-safe copy-assignment
  AbstractFactory& operator=(const AbstractFactory& factory)
  {
    std::scoped_lock lock(m_mutex, factory.m_mutex);
    m_container.clear();
    for (const auto& it : factory.m_container)
      m_container.insert({it.first, std::make_unique<t_factory>(*it.second)});
    return *this;
  }

 private:
  //! \brief Thread-safe copy-ctor implementation
  AbstractFactory(
      const AbstractFactory& factory,
      const std::scoped_lock<std::mutex>&)
  {
    for (const auto& it : factory.m_container)
      m_container.insert({it.first, std::make_unique<t_factory>(*it.second)});
  }

 public:
  //! \brief Create and return factory instance
  //! \tparam t_index Factory instance object
  //! \param key Key mapped to instance
  //! \return Pointer to created instance
  //! \warning Created instances will not throw if created more than once with
  //!   same key
  template <typename t_index>
  std::unique_ptr<t_index> create(const t_key& key)
  {
    std::scoped_lock lock(m_mutex);
    insert<t_index>(key);
    return extract<t_index>(key);
  }

  //! \brief Check-in a factory instance
  //! \note Checked-in instances are stored for later use
  //! \tparam t_index Factory instance object
  //! \param key Key mapped to instance
  //! \throws sekreta::Exception
  template <typename t_index>
  void check_in(const t_key& key)
  {
    std::scoped_lock lock(m_mutex);
    insert<t_index>(key);
  }

  //! \brief Check-out a factory instance
  //! \tparam t_index Factory instance object
  //! \param key Key mapped to instance
  //! \return Pointer to created instance
  //! \throws sekreta::Exception
  //! \warning Checked-out instances are no longer stored within the container
  template <typename t_index>
  std::unique_ptr<t_index> check_out(const t_key& key)
  {
    std::scoped_lock lock(m_mutex);
    return extract<t_index>(key);
  }

  //! \brief Erase factory instance
  //! \tparam t_index Factory instance object
  //! \param key Key mapped to instance
  //! \return True if instance was erased
  template <typename t_index>
  bool erase(const t_key& key)
  {
    std::scoped_lock lock(m_mutex);
    for (auto it = m_container.begin(); it != m_container.end();)
      {
        if (it->first == key && std::holds_alternative<t_index>(*it->second))
          {
            m_container.erase(it);
            return true;
          }
        ++it;
      }
    return false;
  }

  //! \brief Clear the container of all factory objects
  void clear()
  {
    std::scoped_lock lock(m_mutex);
    m_container.clear();
  }

  //! \return Size of the container (count of all the checked-in factory
  //!   objects)
  size_t size() const
  {
    std::scoped_lock lock(m_mutex);
    return m_container.size();
  }

  //! \brief Immutable accessor to container
  const auto& operator()() const noexcept { return m_container; }

 private:
  //! \brief Extract factory into the abstract factory
  template <typename t_index>
  void insert(const t_key& key)
  {
    m_container.insert({key, std::make_unique<t_factory>(t_index{})});
  }

  //! \brief Extract factory from the abstract factory
  template <typename t_index>
  std::unique_ptr<t_index> extract(const t_key& key)
  {
    std::unique_ptr<t_index> factory;

    auto range = m_container.equal_range(key);
    for (auto it = range.first; it != range.second; ++it)
      {
        if (std::holds_alternative<t_index>(*it->second))
          {
            auto node = m_container.extract(it);
            if (!node.empty())
              factory =
                  std::make_unique<t_index>(std::get<t_index>(*node.mapped()));
            break;
          }
      }

    SEK_THROW_IF(!factory, exception::RuntimeError, "Factory unavailable");
    return factory;
  }

 protected:
  //! \brief Base-type mutex
  //! \note May be shared by a derived class
  mutable std::mutex m_mutex;

 private:
  t_container m_container;
};

//! \brief All internal Sekreta components used among all anonymity system APIs.
//!   Not for *direct* public consumption.
//!
//! \note Concurrency is a WIP so you should be considerate of how you use
//!   generic objects and how you fire off functions into threads. Ultimately,
//!   two or more threads *should* be able to invoke a function concurrently on
//!   the same object and it *should* be safe for two or more threads to invoke
//!   a concurrently on the same object.
namespace api
{
//! \brief Shared abstract factory template
//! \tparam t_key Key to store factory with
//! \tparam t_tag Factory tag
//! \tparam t_factory Factory to manage
//! \tparam t_container Container type
template <
    typename t_key,
    typename t_tag,
    typename t_factory,
    template <typename...>
    typename t_container>
class AbstractFactory
{
};

//! \brief Internal API abstract factory / pool
template <typename t_key, typename t_tag>
class AbstractFactory<t_key, t_tag, std::any, std::multimap>
{
  //! \brief Internal alias for comparator-type
  using t_comparator = ::sekreta::internal::Comparator<t_key>;

  //! \brief Internal alias for container-type
  using t_container =
      std::multimap<t_key, std::pair<std::any, t_tag>, t_comparator>;

 protected:
  ~AbstractFactory() = default;

 public:
  //! \brief Check-in and check-out a given instance
  //! \todo Index argument should probably only accept shared_ptr
  //! \tparam t_index Factory index type
  //! \param key Key mapped to instance
  //! \param index Index to check-in and check-out
  //! \returns Pointer to shared instance (index)
  template <typename t_index>
  std::shared_ptr<t_index> create(
      const t_key& key,
      std::shared_ptr<t_index> index = std::make_shared<t_index>())
  {
    std::scoped_lock lock(m_mutex);
    insert<t_index>(key, index);

    auto opt = extract<t_index>(key);
    SEK_THROW_IF(!opt, exception::RuntimeError, "Index not available");

    return opt.value().first;
  }

  //! \brief "Check-in" an instance
  //! \todo Index argument should probably only accept shared_ptr
  //! \tparam t_index Factory index type
  //! \param key Key mapped to instance
  //! \param index Index to check-in
  //! \returns Pointer to shared instance (index)
  template <typename t_index>
  void check_in(
      const t_key& key,
      std::shared_ptr<t_index> index = std::make_shared<t_index>())
  {
    std::scoped_lock lock(m_mutex);
    insert<t_index>(key, index);
  }

  //! \brief "Check-out" an instance from container
  //! \details Removes instance from container
  //! \tparam t_index Factory index type
  //! \param key Key mapped to instance
  //! \returns Pointer to shared instance (index)
  template <typename t_index>
  std::shared_ptr<t_index> check_out(const t_key& key)
  {
    std::scoped_lock lock(m_mutex);

    auto opt = extract<t_index>(key);
    SEK_THROW_IF(!opt, exception::RuntimeError, "Index not available");

    return opt.value().first;
  }

  //! \brief "Borrow" an instance from the container
  //! \details Keeps instance within container
  //! \tparam t_index Factory index type
  //! \param key Key mapped to instance
  //! \returns Pointer to shared instance (index)
  //! \throws sekreta::Exception if index does not exist
  template <typename t_index>
  std::shared_ptr<t_index> borrow(const t_key& key)
  {
    std::scoped_lock lock(m_mutex);
    std::shared_ptr<t_index> index;

    auto opt = extract<t_index>(key);
    SEK_THROW_IF(!opt, exception::RuntimeError, "Index not available");

    index = std::move(opt.value().first);
    m_container.insert({std::move(opt.value().second)});

    return index;
  }

  //! \return Pointer to found instance index, nullptr if not found
  //! \tparam t_index Factory index type
  //! \param key Key mapped to instance
  //! \returns Pointer to shared instance (index)
  //! \note Implementation allows multiple keys with same index
  template <typename t_index>
  std::shared_ptr<t_index> find(const t_key& key)
  {
    std::scoped_lock lock(m_mutex);
    auto range = m_container.equal_range(key);
    for (auto it = range.first; it != range.second; ++it)
      {
        try
          {
            return std::any_cast<std::shared_ptr<t_index>>(it->second.first);
          }
        catch (const std::bad_any_cast& ex)
          {
          }
      }
    return nullptr;
  }

  //! \brief See if container contains instance, given key and index type
  //! \returns True if found, false if not
  //! \note Implementation allows multiple keys with same index
  template <typename t_index>
  bool contains(const t_key& key)
  {
    std::scoped_lock lock(m_mutex);
    auto range = m_container.equal_range(key);
    for (auto it = range.first; it != range.second; ++it)
      {
        try
          {
            std::any_cast<std::shared_ptr<t_index>>(it->second.first);
            return true;
          }
        catch (const std::bad_any_cast& ex)
          {
          }
      }
    return false;
  }

  //! \brief View available instances within factory (along with tag)
  //! \returns Container of all checked-in instance keys and associated tags
  std::multimap<t_key, t_tag, t_comparator> view() const
  {
    std::scoped_lock lock(m_mutex);
    std::multimap<t_key, t_tag, t_comparator> map;
    for (const auto& it : m_container)
      map.insert({it.first, it.second.second});
    return map;
  }

  //! \brief Erase instance given key with index type
  //! \note Implementation allows multiple keys with same index
  template <typename t_index>
  bool erase(const t_key& key)
  {
    std::scoped_lock lock(m_mutex);
    auto range = m_container.equal_range(key);
    for (auto it = range.first; it != range.second; ++it)
      {
        try
          {
            std::any_cast<std::shared_ptr<t_index>>(it->second.first);
            m_container.erase(it);
            return true;
          }
        catch (const std::bad_any_cast& ex)
          {
          }
      }
    return false;
  }

  //! \brief Clear all instances from container
  void clear()
  {
    std::scoped_lock lock(m_mutex);
    m_container.clear();
  }

  //! \brief Count of all instances (container size)
  size_t size() const
  {
    std::scoped_lock lock(m_mutex);
    return m_container.size();
  }

 protected:
  //! \brief Immutable accessor
  const auto& operator()() const noexcept { return m_container; }

  //! \brief Base-type mutex
  //! \note May be shared by a derived class
  mutable std::mutex m_mutex;

 private:
  //! \brief Insert instance into container using index type and key
  template <typename t_index>
  void insert(
      const t_key& key,
      std::shared_ptr<t_index> index = std::make_shared<t_index>())
  {
    SEK_THROW_ASSERT_IF(
        !index, exception::InvalidArgument, "Attempting to insert a nullptr!");
    m_container.insert({key, {index, t_index::type}});
  }

  //! \brief Extract instance from container using index type and key
  //! \return Optional pair of pointer to shared index and node type if found
  //! \note Container node-type used for re-insertion
  template <typename t_index>
  std::optional<
      std::pair<std::shared_ptr<t_index>, typename t_container::node_type>>
  extract(const t_key& key)
  {
    auto range = m_container.equal_range(key);
    for (auto it = range.first; it != range.second; ++it)
      {
        auto node = m_container.extract(it);
        if (!node.empty())
          {
            try
              {
                return {{std::any_cast<std::shared_ptr<t_index>>(
                             node.mapped().first),
                         std::move(node)}};
              }
            catch (const std::bad_any_cast& ex)
              {
              }
          }
      }
    return std::nullopt;
  }

 private:
  t_container m_container;
};

//! \brief Interface to be used with classes that utilize the CRTP
template <typename t_derived>
class CRTP_Basic
{
 public:
  CRTP_Basic() = default;
  virtual ~CRTP_Basic() = default;

  //! \brief Default copy-ctor
  CRTP_Basic(const CRTP_Basic&) = default;

  //! \brief Default copy-assignment
  CRTP_Basic& operator=(const CRTP_Basic&) = default;

  //! \brief Default move-ctor
  CRTP_Basic(CRTP_Basic&&) = default;

  //! \brief Default move-assignment
  CRTP_Basic& operator=(CRTP_Basic&&) = default;

 public:
  //! \brief "That" pointer interface to derived (const)
  //! \details Opposite of "this" pointer
  virtual const t_derived* that() const = 0;

  //! \brief "That" pointer interface to derived
  //! \details Opposite of "this" pointer
  virtual t_derived* that() = 0;
};

//! \brief General director API interface
//! \details Directs ebb and flow of implementations
//! \tparam t_impl Implementation to direct
template <typename t_impl>
class Maestro : private CRTP_Basic<t_impl>
{
 protected:
  Maestro() = default;
  ~Maestro() = default;

  //! \brief Default move-ctor
  Maestro(Maestro&&) = default;

  //! \brief Default move-assignment
  Maestro& operator=(Maestro&&) = default;

  //! \brief Thread-safe copy-ctor
  //! \note Privately implemented
  Maestro(const Maestro& maestro)
      : Maestro(maestro, std::scoped_lock<std::mutex>(maestro.m_mutex))
  {
  }

  //! \brief Thread-safe copy-assignment
  Maestro& operator=(const Maestro& maestro)
  {
    std::scoped_lock lock(m_mutex, maestro.m_mutex);
    // copy data as needed
    return *this;
  }

 private:
  //! \brief Thread-safe copy-ctor implementation
  Maestro(
      SEK_UNUSED const Maestro& maestro,
      const std::scoped_lock<std::mutex>&)
  {
    // copy data as needed
  }

 protected:
  //! \brief Implements const pointer to CRTP-derived (implementation)
  const t_impl* that() const override
  {
    static const auto* const that = static_cast<const t_impl*>(this);
    return that;
  }

  //! \brief Implements pointer to CRTP-derived (implementation)
  t_impl* that() override
  {
    static auto* that = static_cast<t_impl*>(this);
    return that;
  }

 public:
  // Configure

  //! \brief CRTP-derived implementation hook
  template <typename t_ret, typename t_arg>
  t_ret configure(t_arg arg)
  {
    std::scoped_lock lock(m_mutex);
    return that()->template configure_impl<t_ret, t_arg>(arg);
  }

  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret configure()
  {
    std::scoped_lock lock(m_mutex);
    return that()->template configure_impl<t_ret>();
  }

  // Start

  //! \brief CRTP-derived implementation hook
  template <typename t_ret, typename t_arg>
  t_ret start(t_arg arg)
  {
    std::scoped_lock lock(m_mutex);
    return that()->template start_impl<t_ret, t_arg>(arg);
  }

  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret start()
  {
    std::scoped_lock lock(m_mutex);
    return that()->template start_impl<t_ret>();
  }

  // Restart

  //! \brief CRTP-derived implementation hook
  template <typename t_ret, typename t_arg>
  t_ret restart(t_arg arg)
  {
    std::scoped_lock lock(m_mutex);
    return that()->template restart_impl<t_ret, t_arg>(arg);
  }

  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret restart()
  {
    std::scoped_lock lock(m_mutex);
    return that()->template restart_impl<t_ret>();
  }

  // Status

  //! \brief CRTP-derived implementation hook
  template <typename t_ret, typename t_arg>
  t_ret status(t_arg arg)
  {
    std::scoped_lock lock(m_mutex);
    return that()->template status_impl<t_ret, t_arg>(arg);
  }

  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret status()
  {
    std::scoped_lock lock(m_mutex);
    return that()->template status_impl<t_ret>();
  }

  // Stop

  //! \brief CRTP-derived implementation hook
  template <typename t_ret, typename t_arg>
  t_ret stop(t_arg arg)
  {
    std::scoped_lock lock(m_mutex);
    return that()->template stop_impl<t_ret, t_arg>(arg);
  }

  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret stop()
  {
    std::scoped_lock lock(m_mutex);
    return that()->template stop_impl<t_ret>();
  }

 protected:
  //! \brief Base-type mutex
  //! \note May be shared by a derived class
  mutable std::mutex m_mutex;
};

//! \brief Generic path API interface
//! \tparam t_impl Implementation to hook into
//! \todo ASIO callback
template <typename t_impl>
class Path : public Maestro<t_impl>
{
 public:
  virtual ~Path() = default;

  //! \brief Is Path type
  static constexpr bool path = true;

  //! \brief Is Path API type
  static constexpr sekreta::type::kAPI type = sekreta::type::kAPI::Path;
};

//! \brief Generic library API interface
//! \tparam t_impl Implementation to hook into
template <typename t_impl>
class Library : public Maestro<t_impl>
{
  //! \brief Internal alias for base-type
  using t_maestro = Maestro<t_impl>;

 protected:
  ~Library() = default;

 public:
  //! \brief Is Library type
  static constexpr bool library = true;

  //! \brief Is Library API type
  static constexpr sekreta::type::kAPI type = sekreta::type::kAPI::Library;

 public:
  //! \brief Library path implementation hook (w/ arg)
  template <typename t_ret, typename t_arg>
  t_ret path(t_arg arg)
  {
    std::scoped_lock lock(t_maestro::m_mutex);
    return t_maestro::that()->template path_impl<t_ret, t_arg>(arg);
  }

  //! \brief Library path implementation hook (w/out arg)
  template <typename t_ret>
  t_ret path()
  {
    std::scoped_lock lock(t_maestro::m_mutex);
    return t_maestro::that()->template path_impl<t_ret>();
  }
};

//! \brief Generic socket API interface
//! \tparam t_impl Implementation to hook into
//! \todo Finish and merge branch
//! \todo ASIO callback
template <typename t_impl>
class Socket : public Maestro<t_impl>
{
 protected:
  ~Socket() = default;

 public:
  //! \brief Is Socket type
  static constexpr bool socket = true;

  //! \brief Is Socket API type
  static constexpr sekreta::type::kAPI type = sekreta::type::kAPI::Socket;
};

//! \brief Generic plugin API interface
//! \tparam t_impl Implementation to hook into
//! \todo Generic SAM client
//! \todo Generic I2CP client
//! \todo Finish and merge branch
template <typename t_impl>
class Plugin : public Maestro<t_impl>
{
 protected:
  ~Plugin() = default;

 public:
  //! \brief Is Plugin type
  static constexpr bool plugin = true;

  //! \brief Is Plugin API type
  static constexpr sekreta::type::kAPI type = sekreta::type::kAPI::Plugin;
};

//! \brief Generic trait API interface
//! \tparam t_impl Implementation to hook into
//! \todo Finish and merge branch
template <typename t_impl>
class Trait : private CRTP_Basic<t_impl>
{
 protected:
  Trait() = default;
  ~Trait() = default;

  //! \brief Default move-ctor
  Trait(Trait&&) = default;

  //! \brief Default move-assignment
  Trait& operator=(Trait&&) = default;

  //! \brief Thread-safe copy-ctor
  //! \note Privately implemented
  Trait(const Trait& trait)
      : Trait(trait, std::scoped_lock<std::mutex>(trait.m_mutex))
  {
  }

  //! \brief Thread-safe copy-assignment
  Trait& operator=(const Trait& trait)
  {
    std::scoped_lock lock(m_mutex, trait.m_mutex);
    // copy data as needed
    return *this;
  }

 private:
  //! \brief Thread-safe copy-ctor implementation
  Trait(SEK_UNUSED const Trait& trait, const std::scoped_lock<std::mutex>&)
  {
    // copy data as needed
  }

 public:
  //! \brief Is Trait type
  static constexpr bool trait = true;

  //! \brief Is Trait API type
  static constexpr sekreta::type::kAPI type = sekreta::type::kAPI::Trait;

 protected:
  //! \brief Implements const pointer to CRTP-derived (implementation)
  const t_impl* that() const override
  {
    static const auto* const that = static_cast<const t_impl*>(this);
    return that;
  }

  //! \brief Implements pointer to CRTP-derived (implementation)
  t_impl* that() override
  {
    static auto* that = static_cast<t_impl*>(this);
    return that;
  }

 public:
  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret uid()
  {
    std::scoped_lock lock(m_mutex);
    return that()->template uid_impl<t_ret>();
  }

  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret path()
  {
    std::scoped_lock lock(m_mutex);
    return that()->template path_impl<t_ret>();
  }

 protected:
  //! \brief Base-type mutex
  //! \note May be shared by a derived class
  mutable std::mutex m_mutex;
};

//! \brief Generic status API interface
//! \tparam t_impl Implementation to hook into
template <typename t_impl>
class Status : CRTP_Basic<t_impl>
{
 protected:
  Status() = default;
  ~Status() = default;

  //! \brief Default move-ctor
  Status(Status&&) = default;

  //! \brief Default move-assignment
  Status& operator=(Status&&) = default;

  //! \brief Thread-safe copy-ctor
  //! \note Privately implemented
  Status(const Status& status)
      : Status(status, std::scoped_lock<std::mutex>(status.m_mutex))
  {
  }

  //! \brief Thread-safe copy-assignment
  Status& operator=(const Status& status)
  {
    std::scoped_lock lock(m_mutex, status.m_mutex);
    // copy data as needed
    return *this;
  }

 private:
  //! \brief Thread-safe copy-ctor implementation
  Status(SEK_UNUSED const Status& status, const std::scoped_lock<std::mutex>&)
  {
    // copy data as needed
  }

 public:
  //! \brief Is Status type
  static constexpr bool status = true;

  //! \brief Is Status API type
  static constexpr sekreta::type::kAPI type = sekreta::type::kAPI::Status;

 protected:
  //! \brief Implements const pointer to CRTP-derived (implementation)
  const t_impl* that() const override
  {
    static const auto* const that = static_cast<const t_impl*>(this);
    return that;
  }

  //! \brief Implements pointer to CRTP-derived (implementation)
  t_impl* that() override
  {
    static auto* that = static_cast<t_impl*>(this);
    return that;
  }

 public:
  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret version()
  {
    std::scoped_lock lock(m_mutex);
    return that()->template version_impl<t_ret>();
  }

  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret is_running()
  {
    std::scoped_lock lock(m_mutex);
    return that()->template is_running_impl<t_ret>();
  }

  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret state()
  {
    std::scoped_lock lock(m_mutex);
    return that()->template state_impl<t_ret>();
  }

  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret uptime()
  {
    std::scoped_lock lock(m_mutex);
    return that()->template uptime_impl<t_ret>();
  }

  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret inbound_bandwidth()
  {
    std::scoped_lock lock(m_mutex);
    return that()->template inbound_bandwidth_impl<t_ret>();
  }

  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret outbound_bandwidth()
  {
    std::scoped_lock lock(m_mutex);
    return that()->template outbound_bandwidth_impl<t_ret>();
  }

 protected:
  //! \brief Base-type mutex
  //! \note May be shared by a derived class
  mutable std::mutex m_mutex;
};

//! \brief Generic router status API interface
//! \tparam t_impl Implementation to hook into
template <typename t_impl>
class RouterStatus : public Status<t_impl>
{
  //! \brief Internal alias for base-type
  using t_status = Status<t_impl>;

 protected:
  ~RouterStatus() = default;

 public:
  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret data_dir()
  {
    std::scoped_lock lock(t_status::m_mutex);
    return t_status::that()->template data_dir_impl<t_ret>();
  }

  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret path_creation_rate()
  {
    std::scoped_lock lock(t_status::m_mutex);
    return t_status::that()->template path_creation_rate_impl<t_ret>();
  }

  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret path_count()
  {
    std::scoped_lock lock(t_status::m_mutex);
    return t_status::that()->template path_count_impl<t_ret>();
  }

  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret active_peer_count()
  {
    std::scoped_lock lock(t_status::m_mutex);
    return t_status::that()->template active_peer_count_impl<t_ret>();
  }

  //! \brief CRTP-derived implementation hook
  template <typename t_ret>
  t_ret known_peer_count()
  {
    std::scoped_lock lock(t_status::m_mutex);
    return t_status::that()->template known_peer_count_impl<t_ret>();
  }

  //! \brief CRTP-derived implementation hook
  //! \todo This is network-specific
  template <typename t_ret>
  t_ret floodfill_count()
  {
    std::scoped_lock lock(t_status::m_mutex);
    return t_status::that()->template floodfill_count_impl<t_ret>();
  }

  //! \brief CRTP-derived implementation hook
  //! \todo This is network-specific
  template <typename t_ret>
  t_ret leaseset_count()
  {
    std::scoped_lock lock(t_status::m_mutex);
    return t_status::that()->template leaseset_count_impl<t_ret>();
  }
};

//! \brief Generic path status API interface
//! \tparam t_impl Implementation to hook into
template <typename t_impl>
class PathStatus : public Status<t_impl>
{
  //! \brief Internal alias for base-type
  using t_status = Status<t_impl>;

 protected:
  ~PathStatus() = default;

 public:
  // TODO(anonimal):
};

//! \brief Wrappers that "glue" public interfaces to internal implementations
namespace impl_wrapper
{
//! \brief Library wrapper class
//! \todo Implement
template <typename t_impl>
class Library
{
  // no-op
};

//! \brief Library status wrapper class
//! \todo thread-safety
template <typename t_library>
class LibraryStatus
{
 protected:
  //! \brief Construct with pointer to library wrapper
  explicit LibraryStatus(const std::shared_ptr<t_library>& lib) : m_lib(lib)
  {
    SEK_THROW_ASSERT_IF(
        !lib,
        exception::InvalidArgument,
        "Did you initialize through the API?");
  }

 public:
  //! \brief Accessor refrence to pointer to shared library
  const auto& lib() { return m_lib; }

 private:
  std::shared_ptr<t_library> m_lib;
};

//! \brief Path wrapper class
//! \todo thread-safety
template <typename t_library, typename t_path_data, typename t_impl_data>
class LibraryPath
{
 protected:
  //! \brief Construct path with pointer to library wrapper and pointer to path
  //!   data type
  LibraryPath(
      const std::shared_ptr<t_library>& lib,
      const std::shared_ptr<t_path_data>& data)
      : m_lib(lib),
        m_path_data(data),
        m_impl_data(std::make_shared<t_impl_data>())
  {
    SEK_THROW_ASSERT_IF(
        !lib || !data,
        exception::InvalidArgument,
        "Did you initialize through the API?");
  }

 public:
  //! \brief Accessor refrence to pointer to shared library
  const auto& lib() { return m_lib; }

  //! \brief Accessor reference to pointer to shared path data
  const auto& path_data() { return m_path_data; }

  //! \brief Accessor reference to pointer to shared path implementation's data
  const auto& impl_data() { return m_impl_data; }

 private:
  std::shared_ptr<t_library> m_lib;
  std::shared_ptr<t_path_data> m_path_data;
  std::shared_ptr<t_impl_data> m_impl_data;
};

//! \brief Path status wrapper class
//! \todo thread-safety
template <typename t_path>
class LibraryPathStatus
{
 protected:
  //! \brief Construct with pointer to path wrapper
  explicit LibraryPathStatus(const std::shared_ptr<t_path>& path) : m_path(path)
  {
    SEK_THROW_ASSERT_IF(
        !path,
        exception::InvalidArgument,
        "Did you initialize through the API?");
  }

 public:
  //! \brief Accessor reference to pointer to shared path object
  const auto& path() { return m_path; }

 private:
  std::shared_ptr<t_path> m_path;
};

//! \brief Path data wrapper class
//! \tparam t_data Data mix-in class
//! \todo thread-safety
template <typename t_data>
class PathData : virtual public t_data
{
};

}  // namespace impl_wrapper
}  // namespace api
}  // namespace sekreta::internal

#endif  // SRC_GENERIC_HH_
