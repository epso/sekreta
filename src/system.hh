//! \file system.hh
//!
//! \brief Underlying systems' implementation
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright Sekreta License, Version 1.0. See the LICENSE.md file for details

#ifndef SRC_SYSTEM_HH_
#define SRC_SYSTEM_HH_

#include <memory>
#include <mutex>
#include <set>
#include <type_traits>
#include <utility>

#include "error.hh"
#include "generic.hh"
#include "type.hh"

#if defined(KOVRI) && !defined(NOP)
#include "kovri/client/instance.h"
#include "kovri/core/instance.h"
#endif

//! \brief All internal Sekreta components used among all anonymity system APIs.
//!   Not for *direct* public consumption
namespace sekreta::internal::api
{
//! \brief Internal API components used *only* within this anonymity system
namespace kovri
{
//! \brief Wrappers that "glue" public interfaces to internal implementations
//! \details Wrappers provide *interface* abstraction between public APIs and
//!   internal implementations
namespace impl_wrapper
{
//! \brief Library wrapper for a Kovri instance
//! \details The things we must do when RAII is not available...
//! \todo Tests for system using custom impl within our wrappers and
//!   implementing no-ops for those tests
class Library final : public ::sekreta::internal::api::impl_wrapper::Library<
#if defined(KOVRI) && !defined(NOP)
                          ::kovri::client::Instance>
#else
                          void>  // no-op
#endif
{
#if defined(KOVRI) && !defined(NOP)
  using t_core = ::kovri::core::Instance;
  using t_client = ::kovri::client::Instance;
#endif

 public:
  Library() = default;
  ~Library() = default;

  //! \brief This instance should not be copyable (port bindings and
  //!   impl-details prevent desire to ever copy)
  Library(const Library&) = delete;

  //! \brief This instance should not be copyable (port bindings and
  //!   impl-details prevent desire to ever copy)
  Library& operator=(const Library&) = delete;

  //! \brief This instance should not be movable because of the aforementioned
  //!   copy deletion
  Library(Library&&) = delete;

  //! \brief This instance should not be movable because of the aforementioned
  //!   copy deletion
  Library& operator=(Library&&) = delete;

#if defined(KOVRI) && !defined(NOP)
 public:
  //! \brief Initialize library instance
  template <typename t_arg>
  void set(t_arg arg)
  {
    std::scoped_lock lock(m_mutex);
    SEK_THROW_IF(
        m_client, exception::RuntimeError, "Client already configured");
    m_client = std::make_shared<t_client>(*std::make_unique<t_core>(arg));
  }

  //! \brief Reference the initialized library instance
  //! \note There should never be a need to re-seat the instance pointer
  const std::shared_ptr<t_client>& get()
  {
    std::scoped_lock lock(m_mutex);
    SEK_THROW_IF(!m_client, exception::RuntimeError, "Client not configured");
    return m_client;
  }

 private:
  std::shared_ptr<t_client> m_client;
  std::mutex m_mutex;  //!< \todo Recursive!!!
#endif

 public:
  //! \brief Library status interface to wrapper implementation
  //! \todo Tests for system using custom impl within our wrappers and
  //!   implementing no-ops for those tests
  class Status final
      : public ::sekreta::internal::api::impl_wrapper::LibraryStatus<Library>
  {
    //! \brief Internal alias for base-type
    using t_base =
        ::sekreta::internal::api::impl_wrapper::LibraryStatus<Library>;

   public:
    //! \brief Construct with library wrapper. Wrapper should not need to
    //! re-seat the pointer.
    explicit Status(const std::shared_ptr<Library>& lib) : t_base(lib) {}
  };

  //! \brief Path data interface to wrapper implementation
  //! \todo Tests for system using custom impl within our wrappers and
  //!   implementing no-ops for those tests
  class PathData final
#if defined(KOVRI) && !defined(NOP)
      : public ::sekreta::internal::api::impl_wrapper::PathData<
            ::kovri::client::TunnelAttributes>
#endif
  {
  };

  //! \brief Path interface to wrapper implementation
  //! \todo Tests for system using custom impl within our wrappers and
  //!   implementing no-ops for those tests
  //! \todo ASIO callback
  class Path final
      : public ::sekreta::internal::api::impl_wrapper::
            LibraryPath<Library, ::sekreta::type::kovri::PathData, PathData>
  {
    //! \brief Internal alias for base-type
    using t_base =
        LibraryPath<Library, ::sekreta::type::kovri::PathData, PathData>;

   public:
    //! \brief Construct with library path wrapper and sekreta path data fed by
    //!   API. Wrapper should not need to re-seat the pointer.
    Path(
        const std::shared_ptr<Library>& lib,
        const std::shared_ptr<::sekreta::type::kovri::PathData>& data)
        : t_base(lib, data)
    {
    }

   public:
    //! \brief Path status interface to wrapper implementation
    //! \todo Tests for system, using custom impl within our wrappers and
    //!   implementing no-ops for those tests
    class Status final
        : public ::sekreta::internal::api::impl_wrapper::LibraryPathStatus<Path>
    {
      //! \brief Internal alias for base-type
      using t_base =
          ::sekreta::internal::api::impl_wrapper::LibraryPathStatus<Path>;

     public:
      //! \brief Construct with library path wrapper. Wrapper should not need to
      //!   re-seat the pointer.
      explicit Status(const std::shared_ptr<Path>& path) : t_base(path) {}
    };
  };
};
}  // namespace impl_wrapper

//! \brief Implements Kovri library API
//! \details Runs hooks directly into Kovri library API system
//! \todo Tests for system using custom impl within our wrappers and
//!   implementing no-ops for those tests
class Library : public ::sekreta::internal::api::Library<Library>
{
  //! \brief Internal alias for base-type
  using t_system = ::sekreta::type::kSystem;

 protected:
  //! \brief Construct with library wrapper. Wrapper should not need to re-seat
  //! the pointer.
  explicit Library(const std::shared_ptr<impl_wrapper::Library>& impl)
      : m_impl(impl)
  {
  }

 public:
  //! \brief Is a Kovri type
  static constexpr t_system system = t_system::Kovri;

 public:
  //! \brief Implements library instance configuration
  //! \todo Kovri should actually throw with an error message, catch and return
  //!   std::optional of the error message or rethrow with the message
  template <typename t_ret, typename t_arg>
  t_ret configure_impl(t_arg arg)
  {
#if defined(KOVRI) && !defined(NOP)
    m_impl->set<t_arg>(arg);
    m_impl->get()->Initialize();
#else
    return t_ret(arg);  // no-op
#endif
  }

  //! \brief Implements library instance start
  //! \todo Kovri should actually throw with an error message, catch and return
  //!   std::optional of the error message or rethrow with the message
  template <typename t_ret>
  t_ret start_impl()
  {
#if defined(KOVRI) && !defined(NOP)
    m_impl->get()->Start();
#else
    // no-op
#endif
  }

  //! \brief Implements library instance restart
  //! \todo Kovri should actually throw with an error message, catch and return
  //!   std::optional of the error message or rethrow with the message
  template <typename t_ret>
  t_ret restart_impl()
  {
#if defined(KOVRI) && !defined(NOP)
    m_impl->get()->Reload();
#else
    // no-op
#endif
  }

  //! \brief Implements library instance stop
  //! \todo Kovri should actually throw with an error message, catch and return
  //!   std::optional of the error message or rethrow with the message
  template <typename t_ret>
  t_ret stop_impl()
  {
#if defined(KOVRI) && !defined(NOP)
    m_impl->get()->Stop();
#else
    // no-op
#endif
  }

  //! \todo Tests for system, using custom impl within our wrappers and
  //!   implementing no-ops for those tests
  class Status : public ::sekreta::internal::api::RouterStatus<Status>
  {
   protected:
    //! \brief Construct with library status wrapper. Wrapper should not need to
    //!   re-seat the pointer.
    explicit Status(const std::shared_ptr<impl_wrapper::Library::Status>& impl)
        : m_impl(impl)
#if defined(KOVRI) && !defined(NOP)
          ,
          m_status(m_impl->lib()->get()->GetStatus())
#endif
    {
    }

   public:
    //! \brief Implements library instance router check if running
    template <typename t_ret>
    t_ret is_running_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      return m_status->IsRunning();
#else
      return t_ret{};  // no-op
#endif
    }

    //! \brief Implements library instance kovri version
    template <typename t_ret>
    t_ret version_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      return m_status->GetVersion();
#else
      return t_ret{};  // no-op
#endif
    }

    //! \brief Implements library instance data directory
    template <typename t_ret>
    t_ret data_dir_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      return m_status->GetDataPath();
#else
      return t_ret{};  // no-op
#endif
    }

    //! \brief Implements library instance router state
    template <typename t_ret>
    t_ret state_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      return m_status->GetState();
#else
      return t_ret{};  // no-op
#endif
    }

    //! \brief Implements library instance router uptime
    template <typename t_ret>
    t_ret uptime_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      return m_status->GetUptime();  // seconds
#else
      return t_ret{};  // no-op
#endif
    }

    //! \brief Implements library instance router tunnel creation rate
    template <typename t_ret>
    t_ret path_creation_rate_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      return m_status->GetRouterTunnelsCreationRate();
#else
      return t_ret{};  // no-op
#endif
    }

    //! \brief Implements library instance router tunnel count
    template <typename t_ret>
    t_ret path_count_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      return m_status->GetActiveRouterTunnelsCount();
#else
      return t_ret{};  // no-op
#endif
    }

    //! \brief Implements library instance router active peer count
    template <typename t_ret>
    t_ret active_peer_count_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      return m_status->GetActivePeersCount();
#else
      return t_ret{};  // no-op
#endif
    }

    //! \brief Implements library instance router known peer count
    template <typename t_ret>
    t_ret known_peer_count_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      return m_status->GetKnownPeersCount();
#else
      return t_ret{};  // no-op
#endif
    }

    //! \brief Implements library instance router inbound bandwidth in-use
    template <typename t_ret>
    t_ret inbound_bandwidth_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      return m_status->GetInBandwidth();
#else
      return t_ret{};  // no-op
#endif
    }

    //! \brief Implements library instance router outbound bandwidth in-use
    template <typename t_ret>
    t_ret outbound_bandwidth_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      return m_status->GetOutBandwidth();
#else
      return t_ret{};  // no-op
#endif
    }

    //! \brief Implements library instance router floodfill count
    template <typename t_ret>
    t_ret floodfill_count_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      return m_status->GetFloodfillsCount();
#else
      return t_ret{};  // no-op
#endif
    }

    //! \brief Implements library instance router leaseset count
    template <typename t_ret>
    t_ret leaseset_count_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      return m_status->GetLeaseSetsCount();
#else
      return t_ret{};  // no-op
#endif
    }

   private:
    std::shared_ptr<impl_wrapper::Library::Status>
        m_impl;  //!< Wrapper implementation
#if defined(KOVRI) && !defined(NOP)
    std::shared_ptr<::kovri::core::Instance::Status>
        m_status;  //!< Underlying system's implementation
#endif
  };

  //! \brief Implements library instance status
  template <typename t_ret>
  t_ret status_impl()
  {
    using t_type =
        typename std::remove_reference<decltype(*std::declval<t_ret>())>::type;
    return std::make_shared<t_type>(
        std::make_shared<impl_wrapper::Library::Status>(m_impl));
  }

  //! \brief Implements client path (tunnel) API
  //! \todo Tests for system using custom impl within our wrappers and
  //!   implementing no-ops for those tests
  //! \todo ASIO callback
  class Path : public ::sekreta::internal::api::Path<Path>
  {
   protected:
    //! \brief Construct with library path wrapper. Wrapper should not need to
    //!   re-seat the pointer.
    explicit Path(const std::shared_ptr<impl_wrapper::Library::Path>& impl)
        : m_impl(impl)
    {
#if defined(KOVRI) && !defined(NOP)
      namespace type = ::sekreta::type;
      namespace kovri = ::kovri::client;

      using kPathDirection = type::kPathDirection;
      using kPathAffect = type::kPathAffect;

      // TODO(unassigned): thread-safety
      std::shared_ptr<type::kovri::PathData> path = m_impl->path_data();
      std::shared_ptr<impl_wrapper::Library::PathData> tunnel =
          m_impl->impl_data();

      // Since Kovri currently doesn't distinguish between direction and àffect,
      //   its "type" may be overwritten by its àffect
      switch (path->type().direction())
        {
          case kPathDirection::Client:
            {
              tunnel->type = kovri::GetAttribute(kovri::Key::Client);
              switch (path->type().affect())
                {
                  case kPathAffect::Default:
                    break;
                  case kPathAffect::IRC:
                    tunnel->type = kovri::GetAttribute(kovri::Key::IRC);
                    break;
                  default:
                    SEK_THROW(
                        exception::InvalidArgument, "Invalid path àffect");
                    break;
                }
              tunnel->dest = path->remote_point().dest();
              tunnel->dest_port = path->remote_point().port();
              break;
            }
          case kPathDirection::Server:
            {
              tunnel->type = kovri::GetAttribute(kovri::Key::Server);
              switch (path->type().affect())
                {
                  case kPathAffect::Default:
                    break;
                  case kPathAffect::HTTP:
                    tunnel->type = kovri::GetAttribute(kovri::Key::HTTP);
                    break;
                  default:
                    // TODO(unassigned): unsupported. Throw
                    break;
                }
              tunnel->in_port = path->local_point().port().unbound();
              // Can only have a black list or white list but not both (ignore
              //   blacklist if whitelist is given)
              if (!path->acl().white_list().empty())
                {
                  tunnel->acl.list = path->acl().white_list();
                  tunnel->acl.is_white = true;
                }
              else if (!path->acl().black_list().empty())
                {
                  tunnel->acl.list = path->acl().black_list();
                  tunnel->acl.is_black = true;
                }
              break;
            }
          default:
            SEK_THROW(exception::InvalidArgument, "Invalid path direction");
            break;
        }

      tunnel->name = path->uid().name();
      tunnel->address = path->local_point().dest();
      tunnel->port = path->local_point().port().bound();
      tunnel->keys = path->crypto().file();

      m_tunnel = m_impl->lib()->get()->ClientTunnel(*tunnel);
#endif
    }

   public:
    //! \brief Is a Kovri system
    static constexpr t_system system = t_system::Kovri;

   public:
    //! \brief Implements library tunnel configuration
    template <typename t_ret>
    t_ret configure_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      m_tunnel->Configure();
#else
      // no-op
#endif
    }

    //! \brief Implements library tunnel start
    template <typename t_ret>
    t_ret start_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      m_tunnel->Start();
#else
      // no-op
#endif
    }

    //! \brief Implements library tunnel restart
    template <typename t_ret>
    t_ret restart_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      m_tunnel->Restart();
#else
      // no-op
#endif
    }

    //! \brief Implements library tunnel stop
    template <typename t_ret>
    t_ret stop_impl()
    {
#if defined(KOVRI) && !defined(NOP)
      m_tunnel->Stop();
#else
      // no-op
#endif
    }

    //! \brief Tunnel status implementation
    //! \note Kovri tunnel attributes must be set by Path before constructing
    //!   Status (no RAII...)
    //! \todo Tests for system using custom impl within our wrappers and
    //!   implementing no-ops for those tests
    class Status : public ::sekreta::internal::api::PathStatus<Status>
    {
     protected:
      //! \brief Construct with library path status wrapper. Wrapper should not
      //!   need to re-seat the pointer.
      explicit Status(
          const std::shared_ptr<impl_wrapper::Library::Path::Status>& impl)
          : m_impl(impl)
      {
#if defined(KOVRI) && !defined(NOP)
        m_tunnel = m_impl->path()->lib()->get()->ClientTunnelStatus(
            *m_impl->path()->impl_data());
#endif
      }

     public:
      //! \brief Implements library tunnel check if running
      template <typename t_ret>
      t_ret is_running_impl()
      {
#if defined(KOVRI) && !defined(NOP)
        return m_tunnel->IsRunning();
#else
        return t_ret{};  // no-op
#endif
      }

      //! \brief Implements library tunnel uptime
      template <typename t_ret>
      t_ret uptime_impl()
      {
#if defined(KOVRI) && !defined(NOP)
        return m_tunnel->GetUptime();
#else
        return t_ret{};  // no-op
#endif
      }

      //! \brief Implements library tunnel state
      template <typename t_ret>
      t_ret state_impl()
      {
#if defined(KOVRI) && !defined(NOP)
        return m_tunnel->GetState();
#else
        return t_ret{};  // no-op
#endif
      }

      // TODO(unassigned): inbound/outbound bandwidth
      // TODO(unassigned): total bandwidth

     private:
      std::shared_ptr<impl_wrapper::Library::Path::Status>
          m_impl;  //!< Wrapper implementation
#if defined(KOVRI) && !defined(NOP)
      std::shared_ptr<::kovri::client::TunnelStatus>
          m_tunnel;  //!< Underlying system's implementation
#endif
    };

    //! \brief Implements library tunnel status
    template <typename t_ret>
    t_ret status_impl()
    {
      using t_type = typename std::remove_reference<decltype(
          *std::declval<t_ret>())>::type;
      return std::make_shared<t_type>(
          std::make_shared<impl_wrapper::Library::Path::Status>(m_impl));
    }

   private:
    std::shared_ptr<impl_wrapper::Library::Path>
        m_impl;  //!< Wrapper implementation
#if defined(KOVRI) && !defined(NOP)
    std::shared_ptr<::kovri::client::Tunnel>
        m_tunnel;  //!< Underlying system's implementation
#endif
  };

  //! \brief Implements library path (tunnel)
  template <typename t_ret, typename t_arg>
  t_ret path_impl(t_arg arg)
  {
    using t_type =
        typename std::remove_reference<decltype(*std::declval<t_ret>())>::type;
    return std::make_shared<t_type>(
        std::make_shared<impl_wrapper::Library::Path>(m_impl, arg));
  }

 private:
  std::shared_ptr<impl_wrapper::Library> m_impl;
};

class Socket : public ::sekreta::internal::api::Socket<Socket>
{
};

class Plugin : public ::sekreta::internal::api::Plugin<Plugin>
{
};

//! \brief Implements Kovri trait API
class Trait : public ::sekreta::internal::api::Trait<Trait>
{
 public:
  //! \brief Is a Kovri system
  static constexpr ::sekreta::type::kSystem system =
      ::sekreta::type::kSystem::Kovri;

 public:
  //! \brief Implements Kovri UID trait
  template <typename t_ret>
  t_ret uid_impl()
  {
    return std::make_shared<typename std::remove_reference<decltype(
        *std::declval<t_ret>())>::type>();
  }

  //! \brief Implements Kovri tunnel trait
  template <typename t_ret>
  t_ret path_impl()
  {
    return std::make_shared<typename std::remove_reference<decltype(
        *std::declval<t_ret>())>::type>();
  }
};
}  // namespace kovri

//! \todo Finish and merge branch
namespace ire
{
class Library : public ::sekreta::internal::api::Library<Library>
{
};
class Socket : public ::sekreta::internal::api::Socket<Socket>
{
};
class Plugin : public ::sekreta::internal::api::Plugin<Plugin>
{
};
class Trait : public ::sekreta::internal::api::Trait<Trait>
{
};
}  // namespace ire

//! \todo Finish and merge branch
namespace i2p
{
class Library : public ::sekreta::internal::api::Library<Library>
{
};
class Socket : public ::sekreta::internal::api::Socket<Socket>
{
};
class Plugin : public ::sekreta::internal::api::Plugin<Plugin>
{
};
class Trait : public ::sekreta::internal::api::Trait<Trait>
{
};
}  // namespace i2p

//! \todo Finish and merge branch
namespace tor
{
class Library : public ::sekreta::internal::api::Library<Library>
{
};
class Socket : public ::sekreta::internal::api::Socket<Socket>
{
};
class Plugin : public ::sekreta::internal::api::Plugin<Plugin>
{
};
class Trait : public ::sekreta::internal::api::Trait<Trait>
{
};
}  // namespace tor

//! \todo Finish and merge branch
namespace loki
{
class Library : public ::sekreta::internal::api::Library<Library>
{
};
class Socket : public ::sekreta::internal::api::Socket<Socket>
{
};
class Plugin : public ::sekreta::internal::api::Plugin<Plugin>
{
};
class Trait : public ::sekreta::internal::api::Trait<Trait>
{
};
}  // namespace loki

}  // namespace sekreta::internal::api

#endif  // SRC_SYSTEM_HH_
