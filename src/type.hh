//! \file type.hh
//!
//! \brief All types used and provided by Sekreta
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright Sekreta License, Version 1.0. See the LICENSE.md file for details
//!
//! \details Sekreta's type framework utilizes the NPI model. All class types
//!   can be used by any API or underlying anonymity system in a methodical
//!   fashion. A generic verbiage also captures the idioms used across all
//!   systems (all systems require points, ports, paths, etc.)
//!
//! \note All types that *aren't* within the `internal` namespace are public to
//!   all APIs
//!
//! \note Concurrency is a WIP so you should be considerate of how you use type
//!   objects and how you fire off functions into threads. Ultimately, two or
//!   more threads *should* be able to invoke a function concurrently on the
//!   same object and it *should* be safe for two or more threads to invoke a
//!   concurrently on the same object.
//!
//! \todo Don't force reference for types that benefit from copy (string_view,
//!   integral, trivial, etc.)
//!
//! \todo c++20's std::overload (useful enough to wait for the standard)

#ifndef SRC_TYPE_HH_
#define SRC_TYPE_HH_

#include <cstddef>
#include <memory>
#include <mutex>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace sekreta::type
{
//! \brief Supported anonymity systems
enum struct kSystem : uint8_t
{
  Kovri,
  Ire,
  I2P,
  Tor,
  Loki,
  // etc.
};

//! \brief Available API types
enum struct kAPI
{
  Library,
  Socket,
  Plugin,
  Status,
  Trait,
  Path,
};

//! \brief Types for internal usage only. Not for public consumption.
namespace internal
{
//! \brief Base type for pair-like container types
//! \tparam t_first First given type
//! \tparam t_second Second given type
//! \tparam t_container Container type (must hold first and second types)
//! \todo Comparison overloading + thread safety
template <
    typename t_first,
    typename t_second,
    template <typename, typename> typename t_container = std::pair>
class DualType
{
 protected:
  DualType() = default;
  ~DualType() = default;

  //! \brief Construct with separate first and second template types
  DualType(const t_first& first, const t_second& second)
      : m_container(first, second)
  {
  }

  //! \brief Construct with container type
  explicit DualType(const t_container<t_first, t_second>& container)
      : m_container(container)
  {
  }

  //! \brief Default move-ctor
  DualType(DualType&&) = default;

  //! \brief Default move-assignment
  DualType& operator=(DualType&&) = default;

  //! \brief Thread-safe copy-ctor
  //! \note Privately implemented
  DualType(const DualType& type)
      : DualType(type, std::scoped_lock<std::mutex>(type.m_mutex))
  {
  }

  //! \brief Thread-safe copy-assignment
  DualType& operator=(const DualType& type)
  {
    std::scoped_lock lock(m_mutex, type.m_mutex);
    m_container = type.m_container;
    return *this;
  }

 private:
  //! \brief Thread-safe copy-ctor impl
  DualType(const DualType& type, const std::scoped_lock<std::mutex>&)
      : m_container(type.m_container)
  {
  }

 public:
  //! \brief Immutable accessor to container
  const auto& operator()() const noexcept { return m_container; }

 protected:
  //! \brief Container's mutator to first template type
  void first(const t_first& first)
  {
    std::scoped_lock lock(m_mutex);
    m_container.first = first;
  }

  //! \brief Container's mutator to second template type
  void second(const t_second& second)
  {
    std::scoped_lock lock(m_mutex);
    m_container.second = second;
  }

  //! \brief Container's accessor to first template type
  const t_first& first() const
  {
    std::scoped_lock lock(m_mutex);
    return m_container.first;
  }

  //! \brief Container's accessor to second template type
  const t_second& second() const
  {
    std::scoped_lock lock(m_mutex);
    return m_container.second;
  }

 protected:
  //! \brief Base-type mutex
  //! \note May be shared by a derived class
  mutable std::mutex m_mutex;

 private:
  //! \brief DualType container
  t_container<t_first, t_second> m_container;
};

//! \details UID base class facade for DualType
//! \todo Thread safety in comparison overloading
template <
    typename t_name,
    typename t_num,
    template <typename, typename>
    typename t_container>
class UIDBase : virtual public DualType<t_name, t_num, t_container>
{
  // Allows enumerations and UID for name type. Otherwise, should not be
  // arithmetic
  static_assert(
      std::is_arithmetic_v<t_name> ? (
          std::is_enum_v<t_name>
              ? true
              : (std::is_base_of_v<UIDBase<t_name, t_num, t_container>, t_name>
                     ? true
                     : false))
                                   : true);

  // Only integrals and enumerations for num type
  static_assert(!std::is_integral_v<t_num> ? std::is_enum_v<t_num> : true);

  //! \brief Internal alias for base-type
  using t_base = DualType<t_name, t_num, t_container>;

 protected:
  UIDBase() = default;
  ~UIDBase() = default;

  //! \brief Construct with separate name and number types
  explicit UIDBase(const t_name& name, const t_num& num = t_num{})
      : t_base(name, num)
  {
  }

  //! \brief Construct with initialized container type
  explicit UIDBase(const t_container<t_name, t_num>& uid) : t_base(uid) {}

 public:
  //! \brief Is a UID type
  static constexpr bool uid = true;

 public:
  //! \brief Relational overload by name and number
  bool operator==(const UIDBase& rhs) const
  {
    if (this->name() == rhs.name())
      return this->num() == rhs.num();
    return false;
  }

  //! \brief Relational overload by name and number
  bool operator!=(UIDBase& rhs) const { return !operator==(rhs); }

  //! \brief Relational overload by name and number
  bool operator<(UIDBase& rhs) const
  {
    if (this->name() == rhs.name())
      return this->num() < rhs.num();
    return this->name() < rhs.name();
  }

  //! \brief Relational overload by name and number
  bool operator>(UIDBase& rhs) const
  {
    if (this->name() == rhs.name())
      return this->num() > rhs.num();
    return this->name() > rhs.name();
  }

  //! \brief Relational overload by name and number
  bool operator<=(UIDBase& rhs) const { return !operator>(rhs); }

  //! \brief Relational overload by name and number
  bool operator>=(UIDBase& rhs) const { return !operator<(rhs); }

 public:
  //! \brief UID name type mutator
  UIDBase& name(const t_name& name)
  {
    t_base::first(name);
    return *this;
  }

  //! \brief UID number type mutator
  UIDBase& num(const t_num& num)
  {
    t_base::second(num);
    return *this;
  }

  //! \brief UID name type accessor
  const t_name& name() const { return t_base::first(); }

  //! \brief UID number type accessor
  const t_num& num() const { return t_base::second(); }
};
}  // namespace internal

//! \brief Unique identifier
//! \tparam t_name Identifier name
//! \tparam t_num Identifier number
//! \tparam t_uid Container of identifier name and number
template <
    typename t_name = std::string_view,
    typename t_num = uint32_t,
    template <typename, typename> typename t_container = std::pair>
class UID final : public internal::UIDBase<t_name, t_num, t_container>
{
  //! \brief Internal alias for base-type
  using t_dual = internal::DualType<t_name, t_num, t_container>;

  //! \brief Internal alias for base-type
  using t_base = internal::UIDBase<t_name, t_num, t_container>;

 public:
  UID() = default;
  ~UID() = default;

  //! \brief Construct with separate name and number types
  UID(const t_name& name, const t_num& num = t_num{})
      : t_dual(name, num), t_base(name, num)
  {
  }

  //! \brief Construct with initialized container type
  explicit UID(const t_container<t_name, t_num>& uid) : t_dual(uid), t_base(uid)
  {
  }
};

//! \brief Anonymity system unique identifier
//! \tparam t_name Identifier name
//! \tparam t_container Container of identifier name and number
template <typename t_name, template <typename, typename> typename t_container>
class UID<t_name, kSystem, t_container> final
    : public internal::UIDBase<t_name, kSystem, t_container>
{
  //! \brief Internal alias for number-type
  using t_num = kSystem;

  //! \brief Internal alias for base-type
  using t_dual = internal::DualType<t_name, t_num, t_container>;

  //! \brief Internal alias for base-type
  using t_base = internal::UIDBase<t_name, t_num, t_container>;

 public:
  UID() = default;
  ~UID() = default;

  //! \brief Construct with separate name and number types
  UID(const t_name& name, const t_num& num)
      : t_dual(name, num), t_base(name, num)
  {
  }

  //! \brief Construct with initialized container type
  explicit UID(const t_container<t_name, t_num>& uid) : t_dual(uid), t_base(uid)
  {
  }

 public:
  //! \brief Facade for UID number mutator
  UID& system(const t_num& num)
  {
    t_base::second(num);
    return *this;
  }

  //! \brief Facade for UID number accessor
  const t_num& system() const { return t_base::second(); }
};

//! \brief Object representing a socket port (bound) and in-net port (unbound)
//! \tparam t_bound Socket-bound port
//! \tparam t_unbound In-net/anonymity-network port
//! \tparam t_container Container for types
//! \todo Comparison overloading + thread safety
template <
    typename t_bound = uint16_t,
    typename t_unbound = uint16_t,
    template <typename, typename> typename t_container = std::pair>
class Port : virtual public internal::DualType<t_bound, t_unbound, t_container>
{
  // Constrain port to appropriate type (does not prevent functors from
  // returning other integrals)
  static_assert(
      std::is_arithmetic_v<t_bound> ? std::is_same_v<t_bound, uint16_t> : true);
  static_assert(
      std::is_arithmetic_v<t_unbound> ? std::is_same_v<t_unbound, uint16_t>
                                      : true);

  //! \brief Internal alias for base-type
  using t_base = internal::DualType<t_bound, t_unbound, t_container>;

 public:
  Port() = default;
  ~Port() = default;

  //! \brief Construct with separate socket-bound and virtual-bound types
  Port(const t_bound& bound, const t_unbound& unbound) : t_base(bound, unbound)
  {
  }

  //! \brief Construct with socket-bound type only
  explicit Port(const t_bound& bound) : t_base(bound, {}) {}

  //! \brief Construct with initialized container type
  explicit Port(const t_container<t_bound, t_unbound>& point) : t_base(point) {}

 public:
  //! \brief Port socket-bound type mutator
  Port& bound(const t_bound& bound)
  {
    t_base::first(bound);
    return *this;
  }

  //! \brief Port network-bound type mutator
  Port& unbound(const t_unbound& unbound)
  {
    t_base::second(unbound);
    return *this;
  }

  //! \brief Port socket-bound type accessor
  const t_bound& bound() const { return t_base::first(); }

  //! \brief Port network-bound type accessor
  const t_unbound& unbound() const { return t_base::second(); }
};

//! \brief Destination network "point"
//! \details Point can either be local or remote, with inbound or outbound port
//! \tparam t_dest Destination (ident hash, etc.) / hostname / ipv{4,6} / byte
//!   vector / byte array (4, 16), etc.
//! \tparam t_port Destination's port type
//! \tparam t_container Container for types
//! \todo Comparison overloading + thread safety
template <
    typename t_dest = std::string,
    typename t_port = Port<uint16_t, uint16_t>,
    template <typename, typename> typename t_container = std::pair>
class Point : virtual public internal::DualType<t_dest, t_port, t_container>
{
  // Highly unlikely that destination will ever be arthimetic type
  static_assert(!std::is_arithmetic_v<t_dest>);

  // Constrain port to appropriate type (does not prevent functors from
  // returning other integrals)
  static_assert(
      std::is_arithmetic_v<t_port> ? std::is_same_v<t_port, uint16_t> : true);

  //! \brief Internal alias for base-type
  using t_base = internal::DualType<t_dest, t_port, t_container>;

 public:
  Point() = default;
  ~Point() = default;

  //! \brief Construct with separate destination and port types
  Point(const t_dest& dest, const t_port& port) : t_base(dest, port) {}

  //! \brief Construct with destination type only
  explicit Point(const t_dest& dest) : t_base(dest, {}) {}

  //! \brief Construct with initialized container type
  explicit Point(const t_container<t_dest, t_port>& point) : t_base(point) {}

 public:
  //! \brief Point destination type mutator
  Point& dest(const t_dest& dest)
  {
    t_base::first(dest);
    return *this;
  }

  //! \brief Point port type mutator
  Point& port(const t_port& port)
  {
    t_base::second(port);
    return *this;
  }

  //! \brief Point destination type accessor
  const t_dest& dest() const { return t_base::first(); }

  //! \brief Point port type accessor
  const t_port& port() const { return t_base::second(); }

 public:
  //! \brief Facade for destination mutator
  Point& host(const t_dest& host)
  {
    dest(host);
    return *this;
  }

  //! \brief Facade for destination accessor
  const t_dest& host() const { return dest(); }
};

//! \brief Access control lists
//! \tparam t_white_list ACL whitelist type
//! \tparam t_black_list ACL whitelist type
//! \tparam t_container Container for types
//! \todo Comparison overloading + thread safety
template <
    typename t_white_list = std::string_view,
    typename t_black_list = t_white_list,
    template <typename, typename> typename t_container = std::pair>
class ACL
    : virtual public internal::DualType<t_white_list, t_black_list, t_container>
{
  //! \brief Internal alias for base-type
  using t_base = internal::DualType<t_white_list, t_black_list, t_container>;

 public:
  ACL() = default;
  ~ACL() = default;

  //! \brief Construct with separate white and black list types
  ACL(const t_white_list& white_list, const t_black_list& black_list)
      : t_base(white_list, black_list)
  {
  }

  //! \brief Construct with initialized container type
  explicit ACL(const t_container<t_white_list, t_black_list>& acl) : t_base(acl)
  {
  }

 public:
  //! \brief White list type mutator
  ACL& white_list(const t_white_list& white_list)
  {
    t_base::first(white_list);
    return *this;
  }

  //! \brief Black list type mutator
  ACL& black_list(const t_black_list& black_list)
  {
    t_base::second(black_list);
    return *this;
  }

  //! \brief White list type accessor
  const t_white_list& white_list() const { return t_base::first(); }

  //! \brief Black list type accessor
  const t_black_list& black_list() const { return t_base::second(); }
};

//! \brief Enumeration for basic path directions
enum struct kPathDirection : uint8_t
{
  Client,
  Server,
  Inbound,
  Outbound,
};

//! \brief Unique path àffects which paint its implementation type
//! \details Specialized path types, implementation-defined
//! \note This enumerator's *àffect* is dedicated to moneromooo and Howard Chu
enum struct kPathAffect : uint8_t
{
  Default,
  HTTP,
  IRC,
  // etc.
};

//! \brief Path types for anonymity systems
//! \tparam t_direction Direction of tunnel
//! \tparam t_affect The quality or aspect of the path
//! \tparam t_container Container for types
//! \todo Comparison overloading + thread safety
template <
    typename t_direction = kPathDirection,
    typename t_affect = kPathAffect,
    template <typename, typename> typename t_container = std::pair>
class PathType
    : virtual public internal::DualType<t_direction, t_affect, t_container>
{
  //! \brief Internal alias for base-type
  using t_base = internal::DualType<t_direction, t_affect, t_container>;

 public:
  PathType() = default;
  ~PathType() = default;

  //! \brief Construct with separate direction and àffect types
  PathType(const t_direction& direction, const t_affect& affect)
      : t_base(direction, affect)
  {
  }

  //! \brief Construct with initialized container type
  explicit PathType(const t_container<t_direction, t_affect>& acl) : t_base(acl)
  {
  }

 public:
  //! \brief Path direction type mutator
  PathType& direction(const t_direction& direction)
  {
    t_base::first(direction);
    return *this;
  }

  //! \brief Path àffect type mutator
  PathType& affect(const t_affect& affect)
  {
    t_base::second(affect);
    return *this;
  }

  //! \brief Path direction type accessor
  const t_direction& direction() const { return t_base::first(); }

  //! \brief Path àffect type accessor
  const t_affect& affect() const { return t_base::second(); }
};

//! \brief Path hops type
//! \tparam t_count Number count of hops
//! \tparam t_tag Tag associated with hops
template <
    typename t_count = uint8_t,
    typename t_tag = std::string_view,
    template <typename, typename> typename t_container = std::pair>
class HopsType : virtual public internal::DualType<t_count, t_tag, t_container>
{
  //! \brief Internal alias for base-type
  using t_base = internal::DualType<t_count, t_tag, t_container>;

 public:
  HopsType() = default;
  ~HopsType() = default;

  //! \brief Construct with separate count and tag types
  HopsType(const t_count& count, const t_tag& tag) : t_base(count, tag) {}

  //! \brief Construct with initialized container type
  explicit HopsType(const t_container<t_count, t_tag>& acl) : t_base(acl) {}

 public:
  //! \brief Relational overload by hop count and associated tag
  bool operator==(const HopsType& rhs) const
  {
    if (this->count() == rhs.count())
      return this->tag() == rhs.tag();
    return false;
  }

  //! \brief Relational overload by hop count and associated tag
  bool operator!=(HopsType& rhs) const { return !operator==(rhs); }

  //! \brief Relational overload by hop count and associated tag
  bool operator<(HopsType& rhs) const
  {
    if (this->count() == rhs.count())
      return this->tag() < rhs.tag();
    return this->count() < rhs.count();
  }

  //! \brief Relational overload by hop count and associated tag
  bool operator>(HopsType& rhs) const
  {
    if (this->count() == rhs.count())
      return this->tag() > rhs.tag();
    return this->count() > rhs.count();
  }

  //! \brief Relational overload by hop count and associated tag
  bool operator<=(HopsType& rhs) const { return !operator>(rhs); }

  //! \brief Relational overload by hop count and associated tag
  bool operator>=(HopsType& rhs) const { return !operator<(rhs); }

 public:
  //! \brief Hop count type mutator
  HopsType& count(const t_count& count)
  {
    t_base::first(count);
    return *this;
  }

  //! \brief Hop tag type mutator
  HopsType& tag(const t_tag& tag)
  {
    t_base::second(tag);
    return *this;
  }

  //! \brief Hop count type accessor
  const t_count& count() const { return t_base::first(); }

  //! \brief Hop tag type accessor
  const t_tag& tag() const { return t_base::second(); }
};

//! \brief Path hops
//! \tparam t_inbound Inbound hop count
//! \tparam t_outbound Outbound hop count
//! \tparam t_container Hops container
//! \todo Comparison overloading + thread safety
//! \todo Specialize similarly to CryptoKey
template <
    typename t_inbound = HopsType<uint8_t, std::string_view>,
    typename t_outbound = t_inbound,
    template <typename, typename> typename t_container = std::pair>
class Hops
    : virtual public internal::DualType<t_inbound, t_outbound, t_container>
{
  //! \brief Internal alias for base-type
  using t_base = internal::DualType<t_inbound, t_outbound, t_container>;

 public:
  Hops() = default;
  ~Hops() = default;

  //! \brief Construct with separate inbound and outbound
  Hops(const t_inbound& inbound, const t_outbound& outbound)
      : t_base(inbound, outbound)
  {
  }

  //! \brief Construct with inbound only
  explicit Hops(const t_inbound& inbound) : t_base(inbound, {}) {}

  //! \brief Construct with initialized container
  explicit Hops(const t_container<t_inbound, t_outbound>& hops) : t_base(hops)
  {
  }

 public:
  //! \brief Hops inbound mutator
  Hops& inbound(const t_inbound& inbound)
  {
    t_base::first(inbound);
    return *this;
  }

  //! \brief Hops outbound mutator
  Hops& outbound(const t_outbound& outbound)
  {
    t_base::second(outbound);
    return *this;
  }

  //! \brief Hops inbound accessor
  const t_inbound& inbound() const { return t_base::first(); }

  //! \brief Hops outbound accessor
  const t_outbound& outbound() const { return t_base::second(); }
};

namespace crypto
{
//! \brief Enumerator for supported cryptographical types
enum struct kType : uint8_t
{
  Decryption,
  Encryption,
  Keypair,
  Public,
  Secret,
  Signing,
  Symmetric,
};

//! \brief Enumerator for supported cryptographical schemes
enum struct kScheme : uint8_t
{
  AES,
  ChaCha,
  DH,
  DSA,
  ECDSA,
  Ed25519,
  ElGamal,
  Salsa,
  X25519,
  // etc.
};

//! \brief Cryptographical keypair object
//! \tparam t_pk Public key type
//! \tparam t_sk Secret key type
//! \tparam t_container Key container type
//! \todo Comparison overloading + thread safety
template <
    typename t_pk = std::vector<std::byte>,
    typename t_sk = t_pk,
    template <typename, typename> typename t_container = std::pair>
class KeyPair : virtual public internal::DualType<t_pk, t_sk, t_container>
{
  //! \brief Internal alias for base-type
  using t_base = internal::DualType<t_pk, t_sk, t_container>;

 public:
  KeyPair() = default;
  ~KeyPair() = default;

  //! \brief Construct with separate public key and secret key types
  KeyPair(const t_pk& pk, const t_sk& sk) : t_base(pk, sk) {}

  //! \brief Construct with initialized container type
  explicit KeyPair(const t_container<t_pk, t_sk>& key) : t_base(key) {}

 public:
  //! \brief Public key mutator
  KeyPair& pk(const t_pk& pk)
  {
    t_base::first(pk);
    return *this;
  }

  //! \brief Secret key mutator
  KeyPair& sk(const t_sk& sk)
  {
    t_base::second(sk);
    return *this;
  }

  //! \brief Public key accessor
  const t_pk& pk() const { return t_base::first(); }

  //! \brief Secret key accessor
  const t_sk& sk() const { return t_base::second(); }
};

//! \brief Primary template for cryptographical key object
//! \tparam t_data Key data type
//! \todo Comparison overloading + thread safety
template <typename t_data = std::vector<std::byte>>
class Key
{
 public:
  Key() = default;
  ~Key() = default;

  //! \brief Construct with data type
  explicit Key(const t_data& data) : m_data(data) {}

 public:
  //! \brief Immutable data accessor
  const auto& operator()() const noexcept { return m_data; }

  //! \brief Mutable data accessor
  auto& operator()() { return m_data; }

 private:
  t_data m_data;
};

//! \brief Cryptographical key based on keypair type
//! \todo Comparison overloading + thread safety
template <typename t_data>
class Key<KeyPair<t_data>>
{
 public:
  Key() = default;
  ~Key() = default;

  //! \brief Construct with keypair object type
  explicit Key(const KeyPair<t_data>& key) : m_key(key) {}

 public:
  //! \brief Immutable keypair accessor
  const auto& operator()() const noexcept { return m_key; }

  //! \brief Mutable keypair accessor
  auto& operator()() { return m_key; }

 private:
  KeyPair<t_data> m_key;
};

//! \brief Cryptographical key tuple object with associated metadata
//! \tparam t_data Data type (key material type)
//! \tparam t_type Key type (scheme)
//! \tparam t_file Path/filename of stored key material
//! \todo Comparison overloading + thread safety
template <typename t_data, typename t_type, typename t_file>
class Key<std::tuple<t_data, t_type, t_file>>
{
  //! \brief Internal alias for container
  using t_container = std::tuple<t_data, t_type, t_file>;

 public:
  Key() = default;
  ~Key() = default;

  //! \brief Construct with key type only
  explicit Key(const t_type& type) : m_container({}, type, {}) {}

  //! \brief Construct with separate key type and file type
  Key(const t_type& type, const t_file& file) : m_container({}, type, file) {}

  //! \brief Construct with separate data, key, and file type
  Key(const t_data& data, const t_type& type, const t_file& file)
      : m_container(data, type, file)
  {
  }

  //! \brief Construct with initialized container type
  explicit Key(const t_container& container) : m_container(container) {}

  //! \brief Thread-safe copy-ctor
  //! \note Privately implemented
  Key(const Key& type) : Key(type, std::scoped_lock<std::mutex>(type.m_mutex))
  {
  }

 private:
  //! \brief Thread-safe copy-ctor impl
  Key(const Key& type, const std::scoped_lock<std::mutex>&)
      : m_container(type.m_container)
  {
  }

 public:
  //! \brief Thread-safe copy assignment
  Key& operator=(const Key& type)
  {
    std::scoped_lock lock(m_mutex, type.m_mutex);
    m_container = type.m_container;
    return *this;
  }

 public:
  //! \brief Immutable accessor to container
  const auto& operator()() const noexcept { return m_container; };

 public:
  //! \brief Key data type mutator
  //! \note std::get acquires with index because template types may be
  //!   duplicated
  Key& data(const t_data& data)
  {
    std::scoped_lock lock(m_mutex);
    std::get<0>(m_container) = data;
    return *this;
  }

  //! \brief Key scheme type mutator
  //! \note std::get acquires with index because template types may be duplicate
  Key& type(const t_type& type)
  {
    std::scoped_lock lock(m_mutex);
    std::get<1>(m_container) = type;
    return *this;
  }

  //! \brief Key file type mutator
  //! \note std::get acquires with index because template types may be duplicate
  Key& file(const t_file& file)
  {
    std::scoped_lock lock(m_mutex);
    std::get<2>(m_container) = file;
    return *this;
  }

  //! \brief Key data type accessor
  //! \note std::get acquires with index because template types may be duplicate
  const t_data& data() const
  {
    std::scoped_lock lock(m_mutex);
    return std::get<0>(m_container);
  }

  //! \brief Key scheme type accessor
  //! \note std::get acquires with index because template types may be duplicate
  const t_type& type() const
  {
    std::scoped_lock lock(m_mutex);
    return std::get<1>(m_container);
  }

  //! \brief Key file type accessor
  //! \note std::get acquires with index because template types may be duplicate
  const t_file& file() const
  {
    std::scoped_lock lock(m_mutex);
    return std::get<2>(m_container);
  }

 protected:
  //! \brief Class mutex
  mutable std::mutex m_mutex;

 private:
  t_container m_container;
};

//! \brief Cryptographical key superobject
//! \todo Specialize Key class
//! \todo Comparison overloading + thread safety
template <
    typename t_data,
    typename t_type,
    typename t_file,
    template <typename...> typename t_key = std::tuple,
    typename t_meta = std::string_view,
    template <typename, typename> typename t_container = std::pair>
class SuperKey : virtual public internal::DualType<
                     type::crypto::Key<t_key<t_data, t_type, t_file>>,
                     t_meta,
                     t_container>
{
  //! \brief Internal alias for crypto-type
  using t_crypto = type::crypto::Key<t_key<t_data, t_type, t_file>>;

  //! \brief Internal alias for base-type
  using t_base = internal::DualType<t_crypto, t_meta, t_container>;

 public:
  SuperKey() = default;
  ~SuperKey() = default;

  //! \details Key data may be available but not metadata (allows the underlying
  //!   system to implement)
  explicit SuperKey(const t_crypto& crypto) { this->crypto(crypto); }

  //! \details Key metadata may be available but not crypto material (allows the
  //!   underlying system to implement)
  explicit SuperKey(const t_meta& meta) { this->meta(meta); }

  //! \brief Construct with separate crypto and metadata types
  SuperKey(const t_meta& crypto, const t_meta& meta) : t_base(crypto, meta) {}

  //! \brief Construct with container type
  explicit SuperKey(const t_container<t_crypto, t_meta>& crypto)
      : t_base(crypto)
  {
  }

 public:
  //! \brief Crypto type mutator
  SuperKey& crypto(const t_crypto& crypto)
  {
    t_base::first(crypto);
    return *this;
  }

  //! \brief Crypto metadata type mutator
  SuperKey& meta(const t_meta& meta)
  {
    t_base::second(meta);
    return *this;
  }

  //! \brief Crypto type accessor
  const t_crypto& crypto() const { return t_base::first(); }

  //! \brief Crypto metadata type accessor
  const t_meta& meta() const { return t_base::second(); }
};

//! \brief Authentication tag
//! \tparam t_crypto Nonce and key
//! \tparam t_extra Additional/extra data
//! \todo Comparison overloading + thread safety
//! \todo Improve and finish (this is a MAC data structure and not a generator)
template <
    typename t_key = std::vector<std::byte>,
    typename t_nonce = t_key,
    template <typename, typename> typename t_tag = std::pair>
class AuthTag : virtual public internal::DualType<t_key, t_nonce, t_tag>
{
};
}  // namespace crypto

//! \brief Path builder data class for all anonymity paths
//! \todo Comparison overloading + thread safety
template <
    typename t_uid,
    typename t_type,
    typename t_local_point,
    typename t_remote_point,
    typename t_hops,
    typename t_crypto,
    typename t_acl>
class PathData
{
 public:
  PathData() = default;
  ~PathData() = default;

  //! \brief Default move-ctor
  PathData(PathData&&) = default;

  //! \brief Default move-assignment
  PathData& operator=(PathData&&) = default;

  //! \brief Thread-safe copy-ctor
  PathData(const PathData& type)
      : PathData(type, std::scoped_lock<std::mutex>(type.m_mutex))
  {
  }

  //! \brief Thread-safe copy-assignment
  PathData& operator=(const PathData& type)
  {
    std::scoped_lock lock(m_mutex, type.m_mutex);
    m_uid = type.m_uid;
    m_type = type.m_type;
    m_point = type.m_point;
    m_hops = type.m_hops;
    m_crypto = type.m_crypto;
    m_acl = type.m_acl;
    return *this;
  }

 private:
  PathData(const PathData& type, const std::scoped_lock<std::mutex>&)
      : m_uid(type.m_uid),
        m_type(type.m_type),
        m_point(type.m_point),
        m_hops(type.m_hops),
        m_crypto(type.m_crypto),
        m_acl(type.m_acl)
  {
  }

 public:
  //! \brief Path UID type mutator
  void uid(const t_uid& uid)
  {
    std::scoped_lock lock(m_mutex);
    m_uid = uid;
  }

  //! \brief Path type-type mutator
  void type(const t_type& type)
  {
    std::scoped_lock lock(m_mutex);
    m_type = type;
  }

  //! \brief Path local point type mutator
  void local_point(const t_local_point& point)
  {
    std::scoped_lock lock(m_mutex);
    m_point.first = point;
  }

  //! \brief Path remote point type mutator
  void remote_point(const t_remote_point& point)
  {
    std::scoped_lock lock(m_mutex);
    m_point.second = point;
  }

  //! \brief Path hops type mutator
  void hops(const t_hops& hops)
  {
    std::scoped_lock lock(m_mutex);
    m_hops = hops;
  }

  //! \brief Path crypto type mutator
  void crypto(const t_crypto& crypto)
  {
    std::scoped_lock lock(m_mutex);
    m_crypto = crypto;
  }

  //! \brief Path ACL type mutator
  void acl(const t_acl& acl)
  {
    std::scoped_lock lock(m_mutex);
    m_acl = acl;
  }

 public:
  //! \brief Path UID type accessor
  const t_uid& uid() const
  {
    std::scoped_lock lock(m_mutex);
    return m_uid;
  }

  //! \brief Path type-type accessor
  const t_type& type() const
  {
    std::scoped_lock lock(m_mutex);
    return m_type;
  }

  //! \brief Path local point type accessor
  const t_local_point& local_point() const
  {
    std::scoped_lock lock(m_mutex);
    return m_point.first;
  }

  //! \brief Path remote point type accessor
  const t_remote_point& remote_point() const
  {
    std::scoped_lock lock(m_mutex);
    return m_point.second;
  }

  //! \brief Path hops type accessor
  const t_hops& hops() const
  {
    std::scoped_lock lock(m_mutex);
    return m_hops;
  }

  //! \brief Path crypto type accessor
  const t_crypto& crypto() const
  {
    std::scoped_lock lock(m_mutex);
    return m_crypto;
  }

  //! \brief Path ACL type accessor
  const t_acl& acl() const
  {
    std::scoped_lock lock(m_mutex);
    return m_acl;
  }

 protected:
  //! \brief Class mutex
  mutable std::mutex m_mutex;

 private:
  t_uid m_uid{};
  t_type m_type{};
  std::pair<t_local_point, t_remote_point> m_point{};
  t_hops m_hops{};
  t_crypto m_crypto{};
  t_acl m_acl{};
};

namespace internal
{
//! \brief Path builder internal interface
//! \tparam t_path Path type to build with
//! \todo Comparison overloading + thread safety
template <typename t_path>
class Path : virtual public t_path
{
 protected:
  Path() = default;
  ~Path() = default;

  //! \brief Default move-ctor
  Path(Path&&) = default;

  //! \brief Default move-assignment
  Path& operator=(Path&& type) = default;

  //! \brief Thread-safe copy-ctor
  //! \note Privately implemented
  Path(const Path& type)
      : Path(type, std::scoped_lock<std::mutex>(type.m_mutex))
  {
  }

  //! \brief Thread-safe copy assignment
  //! \details Creates new pointer to new object
  Path& operator=(const Path& type)
  {
    std::scoped_lock lock(m_mutex, type.m_mutex);
    m_path = std::make_shared<t_path>(*type.m_path);
    return *this;
  }

 private:
  //! \brief Thread-safe copy-ctor impl
  Path(const Path& type, const std::scoped_lock<std::mutex>&)
      : m_path(std::make_shared<t_path>(*type.m_path))
  {
  }

 public:
  //! \brief Immutable accessor to container
  const auto& operator()() const { return m_path; }

 public:
  //! \brief Build the given path
  //! \details Returns pointer to built path whilst resetting for another build
  std::shared_ptr<t_path> build()
  {
    std::scoped_lock lock(m_mutex, t_path::m_mutex);
    auto ret = std::move(m_path);
    m_path = std::make_shared<t_path>();
    return ret;
  }

 protected:
  //! \brief Resets path object to new path
  void reset()
  {
    std::scoped_lock lock(m_mutex, t_path::m_mutex);
    m_path = std::make_shared<t_path>();
  }

 protected:
  //! \brief Base-type mutex
  //! \note May be shared by a derived class
  mutable std::mutex m_mutex;

 private:
  std::shared_ptr<t_path> m_path;
};
}  // namespace internal

//! \brief Path builder public interface for builder
//! \note Path accessors are only available *after* building the path
//! \todo Comparison overloading + thread safety
template <
    typename t_uid = UID<std::string, uint32_t>,
    typename t_type = PathType<kPathDirection, kPathAffect>,
    typename t_local_point = Point<std::string, Port<uint16_t, uint16_t>>,
    typename t_remote_point = Point<std::string, uint16_t>,
    typename t_hops = Hops<HopsType<uint8_t, std::string>>,
    typename t_crypto = crypto::Key<
        std::tuple<std::vector<std::byte>, crypto::kScheme, std::string>>,
    typename t_acl = ACL<std::string, std::string>>
class Path : virtual public internal::Path<PathData<
                 t_uid,
                 t_type,
                 t_local_point,
                 t_remote_point,
                 t_hops,
                 t_crypto,
                 t_acl>>
{
  //! \brief Internal alias for base-type
  using t_path = internal::Path<PathData<
      t_uid,
      t_type,
      t_local_point,
      t_remote_point,
      t_hops,
      t_crypto,
      t_acl>>;

 public:
  Path() { t_path::reset(); }

 public:
  //! \brief Path UID type mutator
  //! \details Implemented by internal Path builder
  Path& uid(const t_uid& uid)
  {
    t_path::operator()()->uid(uid);
    return *this;
  };

  //! \brief Path type-type mutator
  //! \details Implemented by internal Path builder
  Path& type(const t_type& type)
  {
    t_path::operator()()->type(type);
    return *this;
  };

  //! \brief Path local point type mutator
  //! \details Implemented by internal Path builder
  Path& local_point(const t_local_point& point)
  {
    t_path::operator()()->local_point(point);
    return *this;
  };

  //! \brief Path remote point type mutator
  //! \details Implemented by internal Path builder
  Path& remote_point(const t_remote_point& point)
  {
    t_path::operator()()->remote_point(point);
    return *this;
  };

  //! \brief Path hops type mutator
  //! \details Implemented by internal Path builder
  Path& hops(const t_hops& hops)
  {
    t_path::operator()()->hops(hops);
    return *this;
  };

  //! \brief Path crypto type mutator
  //! \details Implemented by internal Path builder
  Path& crypto(const t_crypto& crypto)
  {
    t_path::operator()()->crypto(crypto);
    return *this;
  };

  //! \brief Path ACL type mutator
  //! \details Implemented by internal Path builder
  Path& acl(const t_acl& acl)
  {
    t_path::operator()()->acl(acl);
    return *this;
  };
};

//! \brief Kovri-specific types for public API
namespace kovri
{
//! \brief Universal identifer type
//! \note Will always be aliased to a Sekreta-framework type
using UID = ::sekreta::type::UID<std::string, uint32_t>;

//! \brief Tunnel type
//! \note Will always be aliased to a Sekreta-framework type
using PathType = ::sekreta::type::
    PathType<::sekreta::type::kPathDirection, ::sekreta::type::kPathAffect>;

//! \brief Port type (local port, remote port)
//! \note Will always be aliased to a Sekreta-framework type
using Port = ::sekreta::type::Port<uint16_t, uint16_t>;

//! \brief Local point type (local destination, port type (bound port / unbound
//!   port (in-net port)))
//! \note Will always be aliased to a Sekreta-framework type
using LocalPoint = ::sekreta::type::Point<std::string, Port>;

//! \brief Remote point type (local destination, port type)
//! \note Will always be aliased to a Sekreta-framework type
using RemotePoint = ::sekreta::type::Point<std::string, uint16_t>;

//! \brief Path hops
//! \note Will always be aliased to a Sekreta-framework type
using Hops = ::sekreta::type::Hops<HopsType<uint8_t, std::string>>;

//! \brief Cryptographical key type
//! \note Will always be aliased to a Sekreta-framework type
using CryptoKey = ::sekreta::type::crypto::Key<
    std::tuple<std::vector<uint8_t>, type::crypto::kScheme, std::string>>;

//! \brief Access control list type
//! \note Will always be aliased to a Sekreta-framework type
using ACL = ::sekreta::type::ACL<std::string>;

//! \brief Path data type used by tunnel/path builder
//! \details Will always be aliased to a data type from Sekreta path builder
using PathData = ::sekreta::type::
    PathData<UID, PathType, LocalPoint, RemotePoint, Hops, CryptoKey, ACL>;

//! \brief Path interface type
//! \note Will always be aliased to a Sekreta-framework type
using Path = ::sekreta::type::
    Path<UID, PathType, LocalPoint, RemotePoint, Hops, CryptoKey, ACL>;

}  // namespace kovri
}  // namespace sekreta::type

#endif  // SRC_TYPE_HH_
